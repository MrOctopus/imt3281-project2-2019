package no.ntnu.imt3281.ludo.client;

import no.ntnu.imt3281.ludo.gui.ChatController;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONObject;

import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>Chat class that keeps track of all rooms/creates new ones.</p>
 */
public class Chat {
    // PRIVATE STATIC VARIABLES
    private static final ConcurrentHashMap<String, ChatController> rooms = new ConcurrentHashMap<>();

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Ask server to join new room.</p>
     * @param name Name of room.
     */
    public static void joinRoom(String name) {
        if (rooms.containsKey(name)) { return; }

        Packet join = new Packet(
                Packet.Type.CHAT,
                Packet.Event.JOIN,
                new JSONObject().put(Packet.ID, name)
        );

        Session.getSingleton().write(join);
    }

    /**
     * <p>Close room with given name.</p>
     * @param name Name of room.
     */
    public static void closeRoom(String name) {
        rooms.remove(name);

        Packet leave = new Packet(
                Packet.Type.CHAT,
                Packet.Event.LEAVE,
                new JSONObject().put(Packet.ID, name)
        );

        Session.getSingleton().write(leave);
    }

    // PROTECTED STATIC INTERFACE
    protected static void listener(Packet packet) throws PacketException {
        String name = packet.data.getString(Packet.ID);

        if (packet.event == Packet.Event.CREATE) {
            createRoom(name, packet);
            return;
        }

        ChatController chat = rooms.get(name);
        if (chat == null) { return; }

        switch (packet.event) {
            case JOIN:
                chat.joinedRoom(packet);
                break;
            case LEAVE:
                chat.leftRoom(packet);
                break;
            case MSG:
                chat.msgRoom(packet);
                break;
            default: throw new PacketException();
        }
    }

    // PRIVATE STATIC INTERFACE
    private static void createRoom(String name, Packet packet) {
        ChatController controller = new ChatController(name, packet);
        controller.fillUsers(packet);
        rooms.put(name, controller);
    }

    // PRIVATE INTERFACE
    private Chat() {}
}

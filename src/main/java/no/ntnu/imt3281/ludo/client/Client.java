package no.ntnu.imt3281.ludo.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.gui.LudoController;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 
 * This is the main class for the client.
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
public class Client extends Application {
	// PUBLIC STATIC VARIABLES
	/**
	 * <p>Current resource bundle used.</p>
	 */
	public static ResourceBundle bundle;

	// PUBLIC STATIC INTERFACE
	/**
	 * <p>Main function used to start the program.</p>
	 * @param args Cmd arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	// PUBLIC INTERFACE
	/**
	 * <p>Override of application start function.</p>
	 * @param primaryStage Primary window.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			bundle = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n", Locale.getDefault());
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"), bundle);
			loader.setController(LudoController.getSingleton());

			primaryStage.setScene(new Scene(loader.load()));
			primaryStage.setTitle(bundle.getString("ludo.title"));
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}

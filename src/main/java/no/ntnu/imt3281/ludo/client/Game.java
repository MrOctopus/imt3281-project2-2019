package no.ntnu.imt3281.ludo.client;

import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONObject;

import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>Game class that keeps track of all games/creates new ones.</p>
 */
public class Game {
    // PRIVATE STATIC VARIABLES
    private static final ConcurrentHashMap<String, GameBoardController> games = new ConcurrentHashMap<>();

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Close game with given id.</p>
     * @param id Id associated with the game.
     */
    public static void closeGame(String id) {
        games.remove(id);

        Packet leave = new Packet(
                Packet.Type.GAME,
                Packet.Event.LEAVE,
                new JSONObject().put(Packet.ID, id)
        );

        Session.getSingleton().write(leave);
    }

    // PROTECTED STATIC INTERFACE
    protected static void listener(Packet packet) throws PacketException {
        String id = packet.data.getString(Packet.ID);

        if (packet.event == Packet.Event.CREATE) {
            createGame(id, packet);
            return;
        }

        GameBoardController game = games.get(id);
        if (game == null) { return; }

        switch (packet.event) {
            case DICE:
                game.throwDice(packet);
                break;
            case PIECE:
                game.movePiece(packet);
                break;
            case LEAVE:
                game.removePlayer(packet);
                break;
            case MSG:
                game.msgRoom(packet);
                break;
            default: throw new PacketException();
        }
    }

    // PRIVATE STATIC INTERFACE
    private static void createGame(String id, Packet packet) {
        Matchmaking.closeWait();
        games.put(id, new GameBoardController(id, games.size()+1, packet));
    }

    // PRIVATE INTERFACE
    private Game() {}
}

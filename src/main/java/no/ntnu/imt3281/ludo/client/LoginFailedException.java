package no.ntnu.imt3281.ludo.client;

/**
 * <p>Exception thrown when login failed.</p>
 */
public class LoginFailedException extends Exception {
    /**
     * <p>Default constructor</p>
     */
    public LoginFailedException() {
        super("Login failed, either because the user doesn't exist or " +
              "the credentials were invalid");
    }
}

package no.ntnu.imt3281.ludo.client;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONObject;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p>Matchmaking class that keeps track of challenges/queued games. Works in combinatio nwith the Game class.</p>
 */
public class Matchmaking {
    // PRIVATE STATIC VARIABLES
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static Alert waitAlert;
    private static Future cooldownFuture;
    private static volatile boolean cooldown = false;

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Close the alert if it is currently open.</p>
     */
    public static void closeWait() {
        if (waitAlert != null) {
            Platform.runLater(()->{
                waitAlert.close();
                waitAlert = null;
            });
        }
    }

    /**
     * <p>Challenge user to a game of ludo.</p>
     * @param username Name of user that is being challenged
     * @return boolean Returns true if challenge succeeded, false if not.
     */
    public static boolean challenge(String username) {
        if (username.equals(Session.getSingleton().getUsername())) { return false; }

        if (cooldown) {
            Alert cooldown = new Alert(
                    Alert.AlertType.NONE, (Client.bundle.getString("ludomatch.wait2")), ButtonType.OK
            );
            cooldown.setTitle((Client.bundle.getString("ludomatch.wait")));
            cooldown.show();
            return false;
        }
        cooldown = true;
        cooldownFuture = scheduler.schedule(()->cooldown = false, 2, TimeUnit.SECONDS);

        Session.getSingleton().write(new Packet(
                Packet.Type.MATCH,
                Packet.Event.CHALLENGE,
                new JSONObject().
                        put(Packet.USER, username)
        ));

        return true;
    }

    /**
     * <p>Join queue to match up with random people.</p>
     */
    public static void joinRandom() {
        waitAlert = new Alert(
                Alert.AlertType.NONE, Client.bundle.getString("ludomatch.waiting"), ButtonType.CLOSE);
        waitAlert.setTitle(Client.bundle.getString("ludomatch.que"));
        waitAlert.setOnCloseRequest(event -> {
            waitAlert = null;
            Packet stopWaiting = new Packet(
                    Packet.Type.MATCH,
                    Packet.Event.CANCEL,
                    null
            );
            Session.getSingleton().write(stopWaiting);
        });

        Packet packet = new Packet(
                Packet.Type.MATCH,
                Packet.Event.JOIN,
                null
        );

        Session.getSingleton().write(packet);
        waitAlert.showAndWait();
    }

    // PROTECTED STATIC INTERFACE
    protected static void listener(Packet packet) throws PacketException {
        switch (packet.event) {
            case FEWPLAYERS:
                tooFewChallenge();
                break;
            case INVALIDUSER:
                invalidChallenge(packet);
                break;
            case CHALLENGE:
                acceptChallenge(packet);
                break;
            case CANCEL:
                closeWait();
                break;
            default: throw new PacketException();
        }
    }

    // PRIVATE STATIC INTERFACE
    private static void invalidChallenge(Packet packet) {
        cooldownFuture.cancel(true);
        cooldown = false;

        Platform.runLater(()->{
            Alert invalid = new Alert(Alert.AlertType.NONE, null, ButtonType.OK);
            invalid.setTitle(Client.bundle.getString("ludomatch.error"));
            Label label = new Label(
                    Client.bundle.getString("ludomatch.challengError")+packet.data.getString(Packet.USER)
            );
            label.setWrapText(true);
            invalid.getDialogPane().setContent(label);
            invalid.show();
        });
    }

    private static void acceptChallenge(Packet packet) {
        Platform.runLater(()->{
            Alert challenge = new Alert(Alert.AlertType.NONE, null, ButtonType.OK, ButtonType.CANCEL);
            challenge.setTitle(Client.bundle.getString("ludomatch.init"));
            Label label = new Label(
                    (Client.bundle.getString("ludomatch.challenge2"))+packet.data.getString(Packet.USER)
            );
            label.setWrapText(true);
            challenge.getDialogPane().setContent(label);

            Optional<ButtonType> result = challenge.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                Packet accept = new Packet(
                        Packet.Type.MATCH,
                        Packet.Event.ACCEPT,
                        new JSONObject().
                                put(Packet.USER, packet.data.get(Packet.USER))
                );

                Session.getSingleton().write(accept);
            }
        });
    }

    private static void tooFewChallenge() {
        Platform.runLater(()->{
            Alert tooFew = new Alert(Alert.AlertType.NONE, null, ButtonType.OK);
            tooFew.setTitle(Client.bundle.getString("ludomatch.toofewtitle"));
            Label label = new Label((Client.bundle.getString("ludomatch.toofew")));
            label.setWrapText(true);
            tooFew.getDialogPane().setContent(label);
            tooFew.showAndWait();
        });
    }

    // PRIVATE INTERFACE
    private Matchmaking() {}
}

package no.ntnu.imt3281.ludo.client;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import no.ntnu.imt3281.ludo.network.Network;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONObject;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Session class, used for communicating with the server. Can only exist as a singleton.</p>
 */
public class Session {
    // PRIVATE STATIC VARIABLES
    private static Session singleton = null;

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Get the singleton object of this class.</p>
     * @return Session singleton object.
     */
    public static Session getSingleton() {
        if (singleton == null) { singleton = new Session(); }
        return singleton;
    }

    // PRIVATE VARIABLES
    private final File tokenFile = new File("session.seskey");
    private final Logger log = Logger.getLogger("Session");

    private Socket connection = null;
    private DataOutputStream bw;
    private DataInputStream br;

    private String username;

    // PUBLIC INTERFACE
    /**
     * <p>Check if the session is connected</p>
     * @return True if connected, false if not.
     */
    public boolean isConnected() {
        return (connection != null);
    }

    /**
     * <p>Try connecting to the server using the info provided in the packet.</p>
     * @param packet Packet containing login/token info.
     * @throws IOException If connection fails to establish.
     * @throws TokenExpiredException If token does not exist on the server side.
     * @throws LoginFailedException If user doesn't exist, or the password is wrong.
     * @throws UsernameTakenException If the username that is registered is already taken.
     */
    public void connect(Packet packet) throws IOException, TokenExpiredException, LoginFailedException, UsernameTakenException {
        if (connection != null) { return; }

        try {
            connection = new Socket(Network.SERVER_HOST, Network.DEFAULT_PORT);
            bw = new DataOutputStream(new BufferedOutputStream(connection.getOutputStream()));
            br = new DataInputStream(new BufferedInputStream(connection.getInputStream()));

            packet.write(bw);
            packet = new Packet(br);

            // Check packet
            switch(packet.event) {
                case TOKEN:
                    if (packet.data == null) { throw new TokenExpiredException(); }
                    break;
                case LOGIN:
                    throw new LoginFailedException();
                case REGISTER:
                    throw new UsernameTakenException();
                default:
                    throw new PacketException();
            }
        }
        catch (Exception e) {
            log.log(Level.INFO, e.getMessage());
            disconnect(false);
            throw e;
        }

        // No exception has been thrown so user has been logged in
        log.log(Level.INFO, "User has been logged in");
        username = packet.data.getString(Packet.USER);
        save(new Packet(
                Packet.Type.SESSION,
                Packet.Event.TOKEN,
                new JSONObject().put(Packet.TOKEN, packet.data.get(Packet.TOKEN))
        ));

        Thread thread = new Thread(this::listener);
        thread.start();
    }

    /**
     * <p>Disconnect from the server, resetting the connection.</p>
     * @param tellServer Boolean, if true tell server that we are disconnecting.
     */
    public void disconnect(boolean tellServer) {
        if (connection == null) { return; }

        try {
            if (tellServer && bw != null) {
                new Packet(Packet.Type.SESSION, Packet.Event.LEAVE, null).write(bw);
            }

            connection.close();
            connection = null;
            bw = null;
            br = null;

            log.log(Level.INFO, "Disconnected successfully");
        } catch (IOException e) {
            log.log(Level.INFO, "Failed to disconnect properly");
        }
    }

    /**
     * <p>Get the username associated with this session.</p>
     * @return Username string.
     */
    public String getUsername() {
        return username;
    }

    /**
     * <p>Write a new packet to the server.</p>
     * @param packet The packet that is to be sent
     */
    public void write(Packet packet) {
        if (connection == null) { return; }

        try {
            packet.write(bw);
        } catch (IOException e) {
            disconnect(false);
        }
    }

    // PRIVATE INTERFACE
    private Session() {
        load();
    }

    /**
     * <p>Saves specified packet to key file.</p>
     * @param packet
     */
    private void save(Packet packet) {
        try {
            if (!tokenFile.isFile()) { tokenFile.createNewFile(); }
            packet.write(new DataOutputStream(new FileOutputStream(tokenFile, false)));
        }
        catch (FileNotFoundException e) {
            log.log(Level.INFO, "Could establish file output stream to file");
        }
        catch (IOException e) {
            log.log(Level.INFO, "File couldn't be made, or output failed to write to file");
        }
    }

    /**
     * <p>Attempt loading session from file.</p>
     */
    private void load() {
        if (tokenFile.isFile()) {
            try {
                connect(new Packet(new DataInputStream(new FileInputStream(tokenFile))));

                Alert alert = new Alert(
                        Alert.AlertType.NONE, Client.bundle.getString("ludosession.loadmsg") , ButtonType.CLOSE
                );
                alert.setTitle(Client.bundle.getString("ludosession.load"));
                alert.showAndWait();
            }
            catch (ConnectException e) {
                log.log(Level.INFO, "Failed to connect to the server.");
            }
            catch (Exception e) {
                log.log(Level.INFO, "Failed to connect using session key. Deleting key file");
                tokenFile.delete();
            }
        }
    }

    private void listener() {
        while (connection != null) {
            try {
                Packet packet = new Packet(br);

                switch (packet.type) {
                    case TABLE:
                        Table.listener(packet);
                        break;
                    case MATCH:
                        Matchmaking.listener(packet);
                        break;
                    case CHAT:
                        Chat.listener(packet);
                        break;
                    case GAME:
                        Game.listener(packet);
                        break;
                    default: throw new PacketException();
                }
            }
            catch (IOException e) {
                log.log(Level.WARNING, e.getMessage());
            }
        }
    }
}

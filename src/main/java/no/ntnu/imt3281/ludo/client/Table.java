package no.ntnu.imt3281.ludo.client;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONArray;

/**
 * <p>Class containing static functions used for retrieving and displaying tables.</p>
 */
public class Table {
    // PUBLIC STATIC INTERFACE
    /**
     * <p>Get a table from the server containing what is specified in the event.</p>
     * @param event Table type.
     */
    public static void getTable(Packet.Event event) {
        Packet packet = new Packet(
                Packet.Type.TABLE,
                event,
                null
        );

        Session.getSingleton().write(packet);
    }

    // PROTECTED STATIC INTERFACE
    protected static void listener(Packet packet) throws PacketException {
        switch (packet.event) {
            case WINS:
                showLeaderboard(packet);
                break;
            case PLAYED:
                showLeaderboard(packet);
                break;
            case ROOMS:
                showRooms(packet);
                break;
            default: throw new PacketException();
        }
    }

    // PRIVATE STATIC INTERFACE
    private static void showLeaderboard(Packet packet) {
        VBox vbox = new VBox();
        vbox.setSpacing(3);
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.TOP_CENTER);

        Label title = new Label(Client.bundle.getString("ludotable."+packet.event.toString()));
        title.setFont(Font.font(null, FontWeight.BOLD, 20));
        vbox.getChildren().add(title);
        vbox.getChildren().add(new Separator());

        JSONArray users = packet.data.getJSONArray(Packet.USERS);
        int length = users.length();

        for (int i = 0; i < length; i += 2) {
            HBox userBox = new HBox();
            userBox.setSpacing(15);
            userBox.setAlignment(Pos.TOP_CENTER);
            String username = users.getString(i);

            Label user = new Label(username+": ");
            user.setFont(Font.font(null, FontWeight.BOLD, 15));
            Label score = new Label(Integer.toString(users.getInt(i+1)));

            userBox.getChildren().add(user);
            userBox.getChildren().add(score);
            vbox.getChildren().add(userBox);
        }

        Platform.runLater(()->{
            Stage stage = new Stage();
            stage.setTitle(Client.bundle.getString("ludotable.title"));
            stage.setResizable(false);
            stage.setScene(new Scene(vbox));
            stage.showAndWait();
        });
    }

    private static void showRooms(Packet packet) {
        VBox vbox = new VBox();
        vbox.setMinWidth(300);
        vbox.setMaxHeight(300);
        vbox.setSpacing(4);
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.TOP_CENTER);

        Label title = new Label(Client.bundle.getString("ludotable.title2"));
        title.setFont(Font.font(null, FontWeight.BOLD, 20));

        ScrollPane scroll = new ScrollPane();
        scroll.setFitToWidth(true);

        TextFlow chats = new TextFlow();
        chats.setLineSpacing(4);
        chats.setTextAlignment(TextAlignment.CENTER);

        scroll.setContent(chats);
        vbox.getChildren().add(title);
        vbox.getChildren().add(new Separator());
        vbox.getChildren().add(scroll);

        for (Object chat : packet.data.getJSONArray(Packet.CHATS)) {
            String strChat = (String) chat;
            Text text = new Text(strChat+"\n");

            text.setFill(Color.DARKBLUE);
            text.setOnMouseEntered(event -> text.setFill(Color.LIGHTBLUE));
            text.setOnMouseClicked(event -> {
                Chat.joinRoom(strChat);
                Stage stage = (Stage) text.getScene().getWindow();
                stage.close();
            });
            text.setOnMouseExited(event -> text.setFill(Color.DARKBLUE));

            chats.getChildren().add(text);
        }

        Platform.runLater(()->{
            Stage stage = new Stage();
            stage.setTitle(Client.bundle.getString("ludotable.title2"));
            stage.setScene(new Scene(vbox));
            stage.showAndWait();
        });
    }

    // PRIVATE INTERFACE
    private Table() {}
}

package no.ntnu.imt3281.ludo.client;

/**
 * <p>Exception thrown when a token has expired.</p>
 */
public class TokenExpiredException extends Exception {
    /**
     * <p>Default constructor</p>
     */
    public TokenExpiredException() {
        super("Token is expired");
    }
}

package no.ntnu.imt3281.ludo.client;

/**
 * <p>Exception thrown when a username is taken.</p>
 */
public class UsernameTakenException extends Exception {
    /**
     * <p>Default constructor</p>
     */
    public UsernameTakenException() {
        super("Username is taken");
    }
}

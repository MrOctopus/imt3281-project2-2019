/**
 * This package includes all purely client related classes
 */
/**
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
package no.ntnu.imt3281.ludo.client;
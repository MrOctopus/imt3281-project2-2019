package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.text.*;
import javafx.scene.paint.*;
import no.ntnu.imt3281.ludo.client.*;
import no.ntnu.imt3281.ludo.network.Packet;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

/**
 * <p>Class used for all chat interactions.</p>
 */
public class ChatController {
    // PRIVATE VARIABLES
    @FXML private TextFlow chatArea;
    @FXML private TextField textToSay;
    @FXML private TextFlow chatUsers;

    private final String name;
    private final HashMap<String, Text> users = new HashMap<>();

    private boolean dropPacket = true;

    // PUBLIC INTERFACE
    /**
     * <p>Constructor for chat controller. Remember to run fillUsers() with
     * the same packet afterwards, or else the tab will not tbe displayed</p>
     * @param name Name of the chat.
     * @param packet Packet containing chat information.
     */
    public ChatController(String name, Packet packet) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"), Client.bundle);
        loader.setController(this);
        this.name = name;

        Platform.runLater(()-> {
            try {
                Tab tab = new Tab(this.name);
                tab.setContent(loader.load());
                tab.setClosable(packet.data.getBoolean(Packet.CLOSE));
                tab.setOnCloseRequest(close -> Chat.closeRoom(this.name));
                LudoController.getSingleton().addTab(tab);
            } catch (IOException e) {
                Chat.closeRoom(name);
                // Do nothing
            }
        });
    }

    /**
     * <p>Function used to fill chatroom users after FMXL elements have been
     * insaminated. Always call this after creating a new Chatcontroller.
     * If you dont, the tab will never be added to the tabbedPane. </p>
     * @param packet Packet containing users/Same packet used in the constructor.
     */
    public void fillUsers(Packet packet) {
        Platform.runLater(()->{
            for (Object username : packet.data.getJSONArray(Packet.USERS)) {
                chatUsers.getChildren().add(newUser((String)username));
            }
        });
    }

    /**
     * <p>Sends joined message, and adds user to user list. The first
     * packet will be dropped, as fillUsers will have already added every
     * existing user upon creating</p>
     * @param packet Packet containing user who joined.
     */
    public void joinedRoom(Packet packet) {
        if (dropPacket) { dropPacket = false; return; }

        String username = packet.data.getString(Packet.USER);
        if (users.containsKey(username)) { return; }

        String system = Client.bundle.getString("ludochat.system");
        String joined = Client.bundle.getString("ludochat.joined");

        Platform.runLater(()->chatUsers.getChildren().add(newUser(username)));

        displayMsg(system, username+" "+joined, Color.GREEN);
    }

    /**
     * <p>Sends leave message, and removes user from the user list.</p>
     * @param packet Packet containing the user who left.
     */
    public void leftRoom(Packet packet) {
        String system = Client.bundle.getString("ludochat.system");
        String left = Client.bundle.getString("ludochat.left");
        String username = packet.data.getString(Packet.USER);

        Platform.runLater(()->chatUsers.getChildren().remove(users.remove(username)));

        displayMsg(system, username+" "+left, Color.RED);
    }

    /**
     * <p>Sends message from a user.</p>
     * @param packet Packet containing the user and message
     */
    public void msgRoom(Packet packet) {
        displayMsg(packet.data.getString(Packet.USER), packet.data.getString(Packet.MSG), Color.BLACK);
    }

    // PRIVATE INTERFACE
    @FXML
    private void sendMsg(ActionEvent event) {
        String msg = textToSay.getText();
        if (msg.isEmpty()) { return; }

        Packet packet = new Packet(
                Packet.Type.CHAT,
                Packet.Event.MSG,
                new JSONObject().
                        put(Packet.ID, name).
                        put(Packet.MSG, msg)
        );

        Session.getSingleton().write(packet);
        textToSay.clear();
    }

    private Text newUser(String username) {
        Text user = new Text(username+"\n");
        user.setFont(Font.font(null, FontPosture.ITALIC, 17));
        user.setFill(Color.DARKBLUE);
        user.setOnMouseEntered(event -> user.setFill(Color.LIGHTBLUE));
        user.setOnMouseClicked(event -> {
            if (Matchmaking.challenge(username)) {
                String system = Client.bundle.getString("ludochat.system");
                String msg = Client.bundle.getString("ludochat.sentchallenge");

                displayMsg(system, msg+username, Color.BLUE);
            }
        });
        user.setOnMouseExited(event -> user.setFill(Color.DARKBLUE));
        users.put(username, user);

        return user;
    }

    private void displayMsg(String username, String msg, Color color) {
        Text user = new Text(username+": ");
        user.setFont(Font.font(null, FontWeight.BOLD, 12));
        user.setFill(color);
        Text message = new Text(msg+"\n");
        message.setFill(color);

        Platform.runLater(()->{
            chatArea.getChildren().add(user);
            chatArea.getChildren().add(message);
        });
    }
}

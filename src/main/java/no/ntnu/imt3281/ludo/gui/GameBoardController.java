package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.client.Game;
import no.ntnu.imt3281.ludo.client.Session;
import no.ntnu.imt3281.ludo.logic.*;
import no.ntnu.imt3281.ludo.network.Packet;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * <p>Class containing all UI logic used in Ludo. Object is stored in a hashmap
 * in the Client/Game.java class.</p>>
 */
public class GameBoardController implements DiceListener, PieceListener, PlayerListener {
    // PRIVATE STATIC CONSTANTS
    private static final int[][] ludoBoard = new int[][]{
            {-1,-1,-1,-1,-1,-1,65,66,67,-1,-1,-1,-1,-1,-1},
            {-1,-1,-1,-1,-1,-1,64,68,16,-1,-1,-1,-1,-1,-1},
            {-1,-1,-1,12,-1,-1,63,69,17,-1,-1,-1,0,-1,-1},
            {-1,-1,13,-1,14,-1,62,70,18,-1,-1,2,-1,1,-1},
            {-1,-1,-1,15,-1,-1,61,71,19,-1,-1,-1,3,-1,-1},
            {-1,-1,-1,-1,-1,-1,60,72,20,-1,-1,-1,-1,-1,-1},
            {54,55,56,57,58,59,-1,73,-1,21,22,23,24,25,26},
            {53,86,87,88,89,90,91,-1,79,78,77,76,75,74,27},
            {52,51,50,49,48,47,-1,85,-1,33,32,31,30,29,28},
            {-1,-1,-1,-1,-1,-1,46,84,34,-1,-1,-1,-1,-1,-1},
            {-1,-1,-1,-1,-1,-1,45,83,35,-1,-1,-1,-1,-1,-1},
            {-1,-1,-1,8,-1,-1,44,82,36,-1,-1,-1,4,-1,-1},
            {-1,-1,9,-1,10,-1,43,81,37,-1,-1,6,-1,5,-1},
            {-1,-1,-1,11,-1,-1,42,80,38,-1,-1,-1,7,-1,-1},
            {-1,-1,-1,-1,-1,-1,41,40,39,-1,-1,-1,-1,-1,-1}};

    // PRIVATE VARIABLES
    @FXML
    private StackPane board;

    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;
    
    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;
    
    @FXML
    private ImageView diceThrown;
    
    @FXML
    private Button throwTheDice;

    @FXML
    private TextFlow chatArea;

    @FXML
    private TextField textToSay;

    @FXML
    private Button sendTextButton;

    private final String id;
    private final Ludo ludo;
    private final ArrayList<Circle[]> pieces;

    // PUBLIC INTERFACE
    /**
     * <p>Controller constructor. Creates a new ludo game, and adds players from packet.</p>
     * @param id String id of the game.
     * @param gameNr Game number. Might not be unique, but it doesn't matter.
     * @param packet Packet containing ludo information.
     */
    public GameBoardController(String id, int gameNr, Packet packet) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"), Client.bundle);
        loader.setController(this);

        this.id = id;
        ludo = new Ludo();

        ludo.addDiceListener(this);
        ludo.addPieceListener(this);
        ludo.addPlayerListener(this);

        try {
            for (Object username : packet.data.getJSONArray(Packet.USERS)) {
                ludo.addPlayer((String)username);
            }
        }
        catch (NoRoomForMorePlayersException e){
            // Won't happen. But IF it does, just ignore it.
        }

        pieces = new ArrayList<>(ludo.nrOfPlayers());

        Platform.runLater(()-> {
            try {
                Tab tab = new Tab(Client.bundle.getString("ludogame.game")+gameNr);
                tab.setContent(loader.load());
                tab.setOnCloseRequest(close -> Game.closeGame(id));
                LudoController.getSingleton().addTab(tab);
            } catch (IOException e) {
                Game.closeGame(id);
            }
        });
    }

        // Sent events
    /**
     * <p>Throw the dice using the information in the packet.</p>
     * @param packet Packet containing dice number.
     */
    public void throwDice(Packet packet) {
        ludo.throwDice(packet.data.getInt(Packet.NR));
    }

    /**
     * <p>Move a piece using the information in the packet.</p>
     * @param packet Packet containing playerN, from pos, to pos.
     */
    public void movePiece(Packet packet) {
        ludo.movePiece(
                packet.data.getInt(Packet.NR),
                packet.data.getInt(Packet.NR+1),
                packet.data.getInt(Packet.NR+2)
        );
    }

    /**
     * <p>Remove a player using the information in the packet.</p>
     * @param packet Packet containing a player number.
     */
    public void removePlayer(Packet packet) {
        ludo.removePlayer(ludo.getPlayerName(packet.data.getInt(Packet.NR)));
    }

    /**
     * <p>Sends message from a user.</p>
     * @param packet Packet containing the user and message
     */
    public void msgRoom(Packet packet) {
        displayMsg(packet.data.getString(Packet.USER), packet.data.getString(Packet.MSG), Color.BLACK);
    }

        // Received events
    /**
     * <p>Sets the dice image to the thrown dice nr.</p>
     * @param event DiceEvent containing dice nr.
     */
    public void diceThrown(DiceEvent event) {
        Platform.runLater(()->diceThrown.setImage(new Image("/images/dice"+event.nr+".png")));
    }

    /**
     * <p>Move the player piece specified in the event.</p>
     * @param event PieceEvent containing player color, piece nr, from pos and to pos.
     */
    public void pieceMoved(PieceEvent event) {
        movePiece(event.color, event.pieceN, event.to);
    }

    /**
     * <p>Update the player status.</p>
     * @param event PlayerEvent containing the changed status.
     */
    public void playerStateChanged(PlayerEvent event) {
        if (event.status == PlayerEvent.WAITING) {
            setPlayerActive(event, false);
            setDiceDisabled(event, true);
        }
        else if (event.status == PlayerEvent.PLAYING) {
            Platform.runLater(()->diceThrown.setImage(new Image("/images/rolldice.png")));
            setPlayerActive(event, true);
            setDiceDisabled(event, ludo.getWinner() != -1);
        }
        else if (event.status == PlayerEvent.WON) {
            setDiceDisabled(event, true);
            Platform.runLater(()->{
                Alert gameDone = new Alert(Alert.AlertType.NONE, null, ButtonType.OK);
                gameDone.setTitle(Client.bundle.getString("ludogame.winner"));
                Label label = new Label(Client.bundle.getString("ludogame.winneris")+ludo.getPlayerName(event.color));
                label.setWrapText(true);
                gameDone.getDialogPane().setContent(label);
                gameDone.showAndWait();
            });
        }
        else if (event.status == PlayerEvent.LEFTGAME) {
            String name = ludo.getPlayerName(event.color);

            Platform.runLater(()->{
                switch (event.color) {
                    case Ludo.RED:
                        player1Name.setText(name);
                        break;
                    case Ludo.BLUE:
                        player2Name.setText(name);
                        break;
                    case Ludo.YELLOW:
                        player3Name.setText(name);
                        break;
                    case Ludo.GREEN:
                        player4Name.setText(name);
                        break;
                    default:
                }

                board.getChildren().removeAll(pieces.get(event.color));
            });
        }
    }

    // PRIVATE INTERFACE
    @FXML
    private void initialize() {
        int size = ludo.nrOfPlayers();
        String me = Session.getSingleton().getUsername();

        for (int i = 0; i < size; ++i) {
            String name = ludo.getPlayerName(i);

            switch (i) {
                case Ludo.RED:
                    boolean iAmRed = me.equals(name);
                    throwTheDice.setDisable(!iAmRed);
                    player1Name.setText(name);
                    placePieces(Color.RED, i, iAmRed);
                    break;
                case Ludo.BLUE:
                    player2Name.setText(name);
                    placePieces(Color.BLUE, i, me.equals(name));
                    break;
                case Ludo.YELLOW:
                    player3Name.setText(name);
                    placePieces(Color.YELLOW, i, me.equals(name));
                    break;
                case Ludo.GREEN:
                    player4Name.setText(name);
                    placePieces(Color.GREEN, i, me.equals(name));
                    break;
                default:
            }
        }
    }

    @FXML
    private void throwDice(ActionEvent event) {
        Session.getSingleton().write(new Packet(
                Packet.Type.GAME,
                Packet.Event.DICE,
                new JSONObject().put(Packet.ID, id)
        ));
    }

    @FXML
    private void sendMsg(ActionEvent event) {
        String msg = textToSay.getText();
        if (msg.isEmpty()) { return; }

        Packet packet = new Packet(
                Packet.Type.GAME,
                Packet.Event.MSG,
                new JSONObject().
                        put(Packet.ID, id).
                        put(Packet.MSG, msg)
        );

        Session.getSingleton().write(packet);
        textToSay.clear();
    }

    private void displayMsg(String username, String msg, Color color) {
        Text user = new Text(username+": ");
        user.setFont(Font.font(null, FontWeight.BOLD, 12));
        user.setFill(color);
        Text message = new Text(msg+"\n");
        message.setFill(color);

        Platform.runLater(()->{
            chatArea.getChildren().add(user);
            chatArea.getChildren().add(message);
        });
    }

    private void setPlayerActive(PlayerEvent event, boolean active) {
        switch (event.color) {
            case Ludo.RED:
                Platform.runLater(()->player1Active.setVisible(active));
                break;
            case Ludo.BLUE:
                Platform.runLater(()->player2Active.setVisible(active));
                break;
            case Ludo.YELLOW:
                Platform.runLater(()->player3Active.setVisible(active));
                break;
            case Ludo.GREEN:
                Platform.runLater(()->player4Active.setVisible(active));
                break;
            default: break;
        }
    }

    private void setDiceDisabled(PlayerEvent event, boolean disable) {
        if (ludo.getPlayerName(event.color).equals(Session.getSingleton().getUsername())) {
            Platform.runLater(()->throwTheDice.setDisable(disable));
        }
    }

    private void placePieces(Color color, int colorN, boolean addClickEvent) {
        Circle[] tmp = new Circle[Player.MAXPIECES];
        pieces.add(tmp);

        for (int i = 0; i < Player.MAXPIECES; ++i) {
            Circle circle = new Circle(20, color);
            circle.setStroke(Color.BLACK);
            circle.setStrokeWidth(2);

            if (addClickEvent) {
                Packet packet = new Packet(
                        Packet.Type.GAME,
                        Packet.Event.PIECE,
                        new JSONObject().
                                put(Packet.ID, id).
                                put(Packet.NR, i)
                );

                circle.setOnMouseClicked(click ->Session.getSingleton().write(packet));
            }

            tmp[i] = circle;
            movePiece(colorN, i, 0);
        }

        board.getChildren().addAll(tmp);
    }

    private void movePiece(int color, int pieceN, int to) {
        Circle piece = pieces.get(color)[pieceN];
        int absPos = Ludo.userGridToLudoBoardGrid(color, to);
        double offset = 0.5;

        if (to == 0) {
            absPos += pieceN;
            offset = 0;
        }

        for (int i = 0; i < ludoBoard.length; ++i) {
            for (int j = 0; j < ludoBoard.length; ++j) {
                if (ludoBoard[i][j] != absPos) { continue; }

                double size = board.getWidth();
                if (size == 0) { size = 722.0; }

                double x = size / ludoBoard.length * (j + offset) - (size / 2);
                double y = size / ludoBoard.length * (i + offset) - (size / 2);

                Platform.runLater(()->{
                    piece.setTranslateX((int)x);
                    piece.setTranslateY((int)y);
                });

                return;
            }
        }
    }
}
package no.ntnu.imt3281.ludo.gui;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.*;
import no.ntnu.imt3281.ludo.network.Packet;
import org.json.JSONObject;

import java.io.IOException;

/**
 * <p>Longin Controller class used for displaying and retrieving login information.</p>
 */
public class LoginController {
    // PRIVATE VARIABLES
    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Text debugmessage;

    private Stage window;

    // PUBLIC INTERFACE
    public LoginController(Stage window) {
        this.window = window;
    }

    // PRIVATE INTERFACE
    @FXML
    private void login(ActionEvent event) {
        connect(Packet.Event.LOGIN);
    }

    @FXML
    private void register(ActionEvent event) {
        connect(Packet.Event.REGISTER);
    }

    private void connect(Packet.Event event) {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (inputValid(username) && inputValid(password)) {
            try {
                JSONObject data = new JSONObject();
                data.put("username", username);
                data.put("password", password);

                Session.getSingleton().connect(new Packet(Packet.Type.SESSION, event, data));
                window.close();

                Alert success = new Alert(
                        Alert.AlertType.NONE, Client.bundle.getString("ludologin.success"), ButtonType.OK
                );
                success.setTitle(Client.bundle.getString("ludologin.login"));
                success.showAndWait();
            }
            catch (IOException e) {
                debugmessage.setText(Client.bundle.getString("ludologin.connectionfailed"));
            }
            catch (TokenExpiredException e) {
                debugmessage.setText(Client.bundle.getString("ludologin.tokenexpired"));
            }
            catch (UsernameTakenException e) {
                debugmessage.setText(Client.bundle.getString("ludologin.usernametaken"));
            }
            catch (LoginFailedException e) {
                debugmessage.setText(Client.bundle.getString("ludologin.invaliduser"));
            }
        }
    }

    private boolean inputValid(String input) {
        if (input == null || input.length() < 1)  {
            debugmessage.setText(Client.bundle.getString("ludologin.notvalid"));
            return false;
        }
        else if(input.length() > 64) {
            debugmessage.setText(Client.bundle.getString("ludologin.notvalid2"));
            return false;
        }
        debugmessage.setText("");
        return true;
    }
}

package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.*;
import no.ntnu.imt3281.ludo.network.Packet;

/**
 * <p>Boss class that keeps track of the main scene events.</p>
 */
public class LudoController {
	// PRIVATE STATIC VARIABLES
	private static LudoController singleton;

	// PUBLIC STATIC INTERFACE
	/**
	 * <p>Get the singleton object of the class.</p>
	 * @return Singleton LudoController object.
	 */
	public static LudoController getSingleton() {
		if (singleton == null) { singleton = new LudoController(); }
		return singleton;
	}

	// PRIVATE VARIABLES
    @FXML
    private MenuItem random;

    @FXML
    private TabPane tabbedPane;

    // PUBLIC INTERFACE
	/**
	 * <p>Adds a new tab to the tabbed pane</p>
	 * @param tab The tab to be added.
	 */
	public void addTab (Tab tab) {
		tabbedPane.getTabs().add(tab);
		tabbedPane.getSelectionModel().select(tab);
	}

	// PRIVATE INTERFACE
	@FXML
	private void startConnection(ActionEvent event) {
		if (Session.getSingleton().isConnected()) { return; }

		FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"), Client.bundle);
		Stage window = new Stage();
		LoginController login = new LoginController(window);
		loader.setController(login);

		try {
			window.setResizable(false);
			window.setTitle(Client.bundle.getString("ludologin.login"));
			window.setScene(new Scene(loader.load()));
			window.showAndWait();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void closeGame(ActionEvent event) {
    	Session.getSingleton().disconnect(true);
    	System.exit(0);
	}

    @FXML
	private void joinRandomGame(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }
		Matchmaking.joinRandom();
	}

	@FXML
	private void challengePlayers(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }

		TextInputDialog challengeplayer = new TextInputDialog();
		challengeplayer.setTitle(Client.bundle.getString("ludomatch.challenge"));
		challengeplayer.setHeaderText(Client.bundle.getString("ludomatch.challengemsg"));
		challengeplayer.setContentText(Client.bundle.getString("ludo.username")+":");
		challengeplayer.setGraphic(null);

		Optional<String> result = challengeplayer.showAndWait();
		result.ifPresent(Matchmaking::challenge);
	}

	@FXML
	private void joinRoom(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }

		TextInputDialog joinchat = new TextInputDialog();
		joinchat.setTitle(Client.bundle.getString("ludochat.join"));
		joinchat.setHeaderText(Client.bundle.getString("ludochat.joinmsg"));
		joinchat.setContentText(Client.bundle.getString("ludochat.chat")+":");
		joinchat.setGraphic(null);

		Optional<String> result = joinchat.showAndWait();
		result.ifPresent(Chat::joinRoom);
	}

	@FXML
	private void listRooms(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }
		Table.getTable(Packet.Event.ROOMS);
	}

	@FXML
	private void mostWins(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }
		Table.getTable(Packet.Event.WINS);
	}

	@FXML
	private void mostPlayed(ActionEvent e) {
		if (!Session.getSingleton().isConnected()) { return; }
		Table.getTable(Packet.Event.PLAYED);
	}

	@FXML
	private void about(ActionEvent e) {
    	Alert about = new Alert(Alert.AlertType.NONE, null, ButtonType.CLOSE);
    	about.setTitle("About");
		Label label = new Label(Client.bundle.getString("ludo.about"));
		label.setWrapText(true);
		about.getDialogPane().setContent(label);
		about.show();
	}

	private LudoController() { }
}

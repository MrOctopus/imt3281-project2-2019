/**
 * This package includes all purely gui related classes/resources
 */
/**
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
package no.ntnu.imt3281.ludo.gui;
package no.ntnu.imt3281.ludo.logic;

import java.util.Objects;

/**
 * <p>DiceEvent class. Catched by implementers of the DiceListener interface.</p>
 */
public class DiceEvent {
    // PUBLIC VARIABLES
    /**
     * <p>Origin board.</p>
     */
    public final Ludo ludo;
    /**
     * <p>Player color.</p>
     */
    public final int color;
    /**
     * <p>Dice number thrown.</p>
     */
    public final int nr;

    // PUBLIC INTERFACE

    /**
     * <p>Constructor for DiceEvents.</p>
     * @param ludo Origin board.
     * @param color Player color.
     * @param nr Thrown dice number.
     */
    public DiceEvent(Ludo ludo, int color, int nr) {
        this.ludo = ludo;
        this.color = color;
        this.nr = nr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiceEvent diceEvent = (DiceEvent) o;
        return color == diceEvent.color &&
                nr == diceEvent.nr &&
                ludo.equals(diceEvent.ludo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ludo, color, nr);
    }
}

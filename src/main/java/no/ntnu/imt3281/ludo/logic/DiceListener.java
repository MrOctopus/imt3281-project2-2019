package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Interface for implementation of DiceListeners</p>
 */
public interface DiceListener {
    void diceThrown(DiceEvent event);
}

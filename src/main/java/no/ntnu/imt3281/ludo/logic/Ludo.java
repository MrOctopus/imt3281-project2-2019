package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * <p>Class for all the Ludo game logic. The class is shared on both the client and server side.</p>
 */
public class Ludo {
    // PUBLIC STATIC CONSTANTS
    /**
     * <p>Constant for the maximum allowed players.</p>
     */
    public static final int MAXPLAYERS = 4;
    /**
     * <p>Constants for the different player colors.</p>
     */
    public static final int
            RED = 0,
            BLUE = 1,
            YELLOW = 2,
            GREEN = 3;

    // PRIVATE STATIC CONSTANTS
    private static final Random RANDOM = new Random();

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Converts player piece positions to ludo board grid positions.</p>
     * @param playerN Player number/color.
     * @param pos Local position.
     * @return Ludogrid position int.
     */
    public static int userGridToLudoBoardGrid(int playerN, int pos) {
        if (pos == 0) { return playerN * 4; }
        if (53 < pos && pos < 60) { return playerN * 6 + 14 + pos; }

        pos = playerN * 13 + 15 + pos;
        return (pos <= 67) ? pos : (pos % 67) + 15;
    }

    // PRIVATE VARIABLES
    private List<Player> players = new ArrayList<>(MAXPLAYERS);
    private List<DiceListener> diceListeners = new ArrayList<>();
    private List<PieceListener> pieceListeners = new ArrayList<>();
    private List<PlayerListener> playerListeners = new ArrayList<>();

    private String status;

    private int winner = -1;
    private int activePlayer = RED;
    private int lastThrow;
    private boolean mustMove;

        // Check move is only useful in cases where the skipPlayer test function is used.
        // In the future, ludoTest should use the native skipPlayer function.
    private boolean checkMove;

    // PUBLIC INTERFACE
    /**
     * <p>Default constructor. Sets status to Created.</p>
     */
    public Ludo() {
        status = "Created";
    }

    /**
     * <p>Constructor with a set amount of players. Sets status to initiated.</p>
     * @param p1Name Player 1, name.
     * @param p2Name Player 2, name.
     * @param p3Name Player 3, name.
     * @param p4Name Player 4, name.
     * @throws NotEnoughPlayersException If less than 2 players are provided as arguments.
     */
    public Ludo(String p1Name, String p2Name, String p3Name, String p4Name) throws NotEnoughPlayersException {
        players.add(new Player(p1Name, 0));
        players.add(new Player(p2Name, 1));
        players.add(new Player(p3Name, 2));
        players.add(new Player(p4Name,3 ));

        if (nrOfPlayers() < 2) { throw new NotEnoughPlayersException(); }

        status = "Initiated";
    }

    /**
     * <p>Add player with given name to the game.</p>
     * @param name Name of the player.
     * @throws NoRoomForMorePlayersException If the player ArrayList is full.
     */
    public void addPlayer(String name) throws NoRoomForMorePlayersException {
        if (players.size() >= MAXPLAYERS) { throw new NoRoomForMorePlayersException(); }
        if (status.equals("Started")) { return; }

        players.add(new Player(name, players.size()));
        status = "Initiated";
    }

    /**
     * <p>Remove the player with the given name from the game.</p>
     * @param name Name of the player.
     */
    public void removePlayer(String name) {
        Iterator<Player> iterator = players.iterator();
        Player nextP;

        while (iterator.hasNext()) {
            nextP = iterator.next();

            if (nextP.getName().equals(name)) {
                int playerN = players.indexOf(nextP);
                nextP.setStatus(false);

                Iterator<PlayerListener> playerIterator = playerListeners.iterator();
                while (playerIterator.hasNext()) {
                    playerIterator.next().playerStateChanged(new PlayerEvent(this, playerN, PlayerEvent.LEFTGAME));
                }

                if (playerN == activePlayer) { advanceTurn(playerN); }
                break;
            }
        }
    }

    /**
     * <p>Get the player name of the given color/player number.</p>
     * @param pN Player color/number
     * @return Name, or Inactive + name if the player has left.
     */
    public String getPlayerName(Integer pN) {
        Player player = players.get(pN);
        return (player.getStatus().equals("Inactive")) ? "Inactive: "+player.getName() : player.getName();
    }

    /**
     * <p>Nr of players that exist in the game.</p>
     * @return Number of players.
     */
    public Integer nrOfPlayers() {
        Iterator<Player> iterator = players.iterator();
        int numP = 0;

        while (iterator.hasNext()) {
            if (iterator.next().getName() != null) { ++numP; }
        }

        return numP;
    }

    /**
     * <p>The numbers of players that are still active within the game.</p>
     * @return Number of active players.
     */
    public Integer activePlayers() {
        Iterator<Player> iterator = players.iterator();
        int numP = 0;

        while (iterator.hasNext()) {
            Player nextP = iterator.next();
            if (nextP.getStatus().equals("Active") && nextP.getName() != null) { ++numP; }
        }

        return numP;
    }

    /**
     * <p>The currently active player.</p>
     * @return Active player int.
     */
    public int activePlayer() {
        return activePlayer;
    }

    /**
     * <p>The local position of the piece of the given player number/color.</p>
     * @param pN Player number/color.
     * @param piece Piece number.
     * @return Position int.
     */
    public int getPosition(int pN, int piece) {
        return players.get(pN).getPiece(piece).getPosition();
    }

    /**
     * <p>Throw the dice and generate a random number between 1 and 6</p>
     * @return 1 to 6 int
     */
    public int throwDice() {
        checkMove = true;
        return RANDOM.nextInt(6) + 1;
    }

    /**
     * <p>Get the last thrown number.</p>
     * @return lastThrow (1 -6 int).
     */
    public int getLastThrow() {
        return lastThrow;
    }

    /**
     * <p>Get the current status of the game.</p>
     * @return Finished, Initiated, Started, Created.
     */
    public String getStatus() {
        return status;
    }

    /**
     * <p>Get the winner of the game.</p>
     * @return The winner, -1 if no winner has been declared.
     */
    public int getWinner() {
        return winner;
    }

    /**
     * <p>Throw the dice with the provided number.</p>
     * @param num Dice number.
     * @return The thrown number.
     */
    public int throwDice(int num) {
        if (mustMove && checkMove) { return num; }
        if (!status.equals("Started")) { status = "Started"; }

        Player player = players.get(activePlayer);

        Iterator<DiceListener> diceLisIterator = diceListeners.iterator();
        while (diceLisIterator.hasNext()) {
            diceLisIterator.next().diceThrown(new DiceEvent(this, activePlayer, num));
        }

        if (player.piecesInGame() == 0) {
            int attempts = player.throwDiceAttempt(num);

            if (attempts == 3) {
                advanceTurn(activePlayer);
            }
            else if (attempts == 0) {
                mustMove = true;
            }
        }
        else if (player.throwDice(num) || skipTurn(player, num)) {
            advanceTurn(activePlayer);
        }
        else {
            mustMove = true;
        }

        checkMove = false;
        lastThrow = num;
        return num;
    }

    /**
     * <p>Moves player piece located at from pos to pos.</p>
     * @param playerN Player number/color of moving player.
     * @param from From position.
     * @param to To position.
     * @return False if move failed, true if succeeded.
     */
    public boolean movePiece(int playerN, int from, int to) {
        // Error handling
        if (lastThrow == 0 || from == Piece.FINISH || to > Piece.FINISH) { return false; }

        Player player = players.get(playerN);
        int pieceN = player.getPieceNum(from);

        if (pieceN == -1) { return false; }
        if (from == 0 && to != 1) { return false; }
        if (isTowerBlocking(player, from, to-from)) { return false; }

        // Piece can move
        Iterator<PieceListener> pieceLisIterator = pieceListeners.iterator();
        while (pieceLisIterator.hasNext()) {
            pieceLisIterator.next().pieceMoved(new PieceEvent(this, playerN, pieceN, from, to));
        }

        moveOtherPlayers(player, to);

        if (lastThrow != 6 || (from == 0 && player.piecesInGame() == 0)) {
            advanceTurn(playerN);
        }
        else {
            lastThrow = 0;
            mustMove = false;
        }

        player.getPiece(pieceN).setPosition(to);
        if (player.isFinished()) {
            status = "Finished";
            winner = playerN;

            Iterator<PlayerListener> playerLisIterator = playerListeners.iterator();
            while (playerLisIterator.hasNext()) {
                playerLisIterator.next().playerStateChanged(new PlayerEvent(this, playerN, PlayerEvent.WON));
            }
        }

        return true;
    }

    /**
     * <p>Add a new dice listener to the game.</p>
     * @param listener The listener to add.
     */
    public void addDiceListener(DiceListener listener) {
        diceListeners.add(listener);
    }

    /**
     * <p>Add a new piece listener to the game.</p>
     * @param listener The listener to add.
     */
    public void addPieceListener(PieceListener listener) {
        pieceListeners.add(listener);
    }

    /**
     * <p>Add a new player listener to the game.</p>
     * @param listener The listener to add.
     */
    public void addPlayerListener(PlayerListener listener) {
        playerListeners.add(listener);
    }

    // PRIVATE INTERFACE
    private boolean skipTurn(Player player, int num) {
        int cantMove = 0;

        for (int i = 0; i < Player.MAXPIECES; ++i) {
            Piece piece = player.getPiece(i);

            if (!piece.getInGame()) { continue; }

            int pos = piece.getPosition();

            if ((pos == 0 && num != 6) ||
                (pos + num > Piece.FINISH) ||
                isTowerBlocking(player, pos, (pos == 0) ? 1 : num)
            ) { ++cantMove; }
        }

        return (cantMove >= player.piecesInGame());
    }

    private boolean isTowerBlocking(Player player, int from, int num) {
        int size = nrOfPlayers();
        int pos = userGridToLudoBoardGrid(player.getColor(), from);

        for (int i = 0; i < size; ++i) {
            Player player2 = players.get(i);
            if (player == player2) { continue; }

            for (int j = 1; j <= num; ++j) {
                if (player2.getPiecesAtAbs(pos+j) > 1) { return true; }
            }
        }

        return false;
    }

    private void moveOtherPlayers(Player player, int to) {
        int size = nrOfPlayers();
        int pos = userGridToLudoBoardGrid(player.getColor(), to);

        for (int i = 0; i < size; ++i) {
            Player player2 = players.get(i);
            if (player == player2) { continue; }

            if (player2.getPiecesAtAbs(pos) == 1) {
                int pieceN = player2.getPieceNumAbs(pos);
                Piece piece = player2.getPiece(pieceN);

                Iterator<PieceListener> pieceLisIterator = pieceListeners.iterator();
                while (pieceLisIterator.hasNext()) {
                    pieceLisIterator.next().pieceMoved(new PieceEvent(
                            this,
                            player2.getColor(),
                            pieceN,
                            piece.getPosition(),
                            0)
                    );
                }

                piece.setPosition(0);
                return;
            }
        }
    }

    private void advanceTurn(int playerN) {
        lastThrow = 0;
        mustMove = false;
        int size = nrOfPlayers();

        for (int i = 0; i < size; ++i) {
            activePlayer = (activePlayer + 1) % size;
            Player player = players.get(activePlayer);
            if (player.getName() != null && player.getStatus().equals("Active")) { break; }
        }

        Iterator<PlayerListener> playerLisIterator = playerListeners.iterator();
        while (playerLisIterator.hasNext()) {
            PlayerListener playerListener = playerLisIterator.next();
            playerListener.playerStateChanged(new PlayerEvent(this, playerN, PlayerEvent.WAITING));
            playerListener.playerStateChanged(new PlayerEvent(this, activePlayer, PlayerEvent.PLAYING));
        }
    }
}


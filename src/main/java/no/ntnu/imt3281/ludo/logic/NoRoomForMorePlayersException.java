package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Exception thrown if the player limit is tried exceeded when adding players
 * to a ludo game.</p>
 */
public class NoRoomForMorePlayersException extends Exception {
    /**
     * <p>Default constructor.</p>
     */
    public NoRoomForMorePlayersException() {
        super("Game is full");
    }
}

package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Exception thrown if a ludo game is attempted started without
 * enough players.</p>
 */
public class NotEnoughPlayersException extends Exception {
    /**
     * <p>Default constructor.</p>
     */
    public NotEnoughPlayersException() {
        super("Not enough players to start a game");
    }
}

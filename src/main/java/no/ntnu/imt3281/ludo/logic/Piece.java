package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Piece class that keeps track of all piece related data.</p>
 */
public class Piece {
    // PUBLIC STATIC CONSTANTS
    /**
     * <p>Constant for the finish line.</p>
     */
    public static final int FINISH = 59;

    // PRIVATE VARIABLES
    private int position;

    // PUBLIC INTERFACE
    /**
     * <p>Piece constructor. Default position is 0.</p>
     */
    public Piece() {
        this.position = 0;
    }

    /**
     * <p>Get the local board position of this piece.</p>
     * @return Position int.
     */
    public int getPosition() {
        return position;
    }

    /**
     * <p>Set the local board position of this piece.</p>
     * @param position Position int.
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * <p>See if this piece is on the game board.</p>
     * @return True if on the board, false if not.
     */
    public boolean getInGame() {
        return (position != 0 && position != FINISH);
    }

    /**
     * <p>Check if this piece is finished.</p>
     * @return True if finished, false if not.
     */
    public boolean getFinished() {
        return (position == FINISH);
    }
}

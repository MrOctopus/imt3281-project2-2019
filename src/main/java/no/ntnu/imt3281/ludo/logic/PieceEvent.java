package no.ntnu.imt3281.ludo.logic;

import java.util.Objects;

/**
 * <p>PieceEvent class. Catched by implementers of the PieceListener interface.</p>
 */
public class PieceEvent {
    // PUBLIC VARIABLES
    /**
     * <p>Origin board.</p>
     */
    public final Ludo ludo;
    /**
     * <p>Player color.</p>
     */
    public final int color;
    /**
     * <p>Piece number.</p>
     */
    public final int pieceN;
    /**
     * <p>From position.</p>
     */
    public final int from;
    /**
     * <p>To position.</p>
     */
    public final int to;

    // PUBLIC INTERFACE
    /**
     * <p>Constructor for PieceEvent</p>
     * @param ludo Origin board.
     * @param color Color of the player.
     * @param pieceN Piece number.
     * @param from From position.
     * @param to To position.
     */
    public PieceEvent(Ludo ludo, int color, int pieceN, int from, int to) {
        this.ludo = ludo;
        this.color = color;
        this.pieceN = pieceN;
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PieceEvent that = (PieceEvent) o;
        return color == that.color &&
                pieceN == that.pieceN &&
                from == that.from &&
                to == that.to &&
                ludo.equals(that.ludo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ludo, color, pieceN, from, to);
    }
}

package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Interface for implementation of PieceListeners</p>
 */
public interface PieceListener {
    void pieceMoved(PieceEvent event);
}

package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>Player class that keeps track of all player data.</p>
 */
public class Player {
    // PUBLIC STATIC CONSTANTS
    /**
     * <p>Constant for the max amount of pieces a player can have.</p>
     */
    public static final int MAXPIECES = 4;

    // PRIVATE VARIABLES
    private final String name;
    private final int color;
    private String status;
    private int attempts = 0;
    private int sixInRow = 0;

    private List<Piece> pieces = new ArrayList<>(MAXPIECES);

    // PUBLIC INTERFACE

    /**
     * <p>Player constructor. Default status is Active.</p>
     * @param name Name of the player.
     * @param color Color/Number of the player.
     */
    public Player(String name, int color) {
        this.name = name;
        setStatus(true);
        this.color = color;

        for (int i = 0; i < MAXPIECES; ++i) {
            pieces.add(new Piece());
        }
    }

    /**
     * <p>Get the name of the player.</p>
     * @return Name string.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Get the player number/color.</p>
     * @return Color/Player number.
     */
    public int getColor() {
        return color;
    }

    /**
     * <p>Current status of the player.</p>
     * @return Status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * <p>Get the piece with the given number.</p>
     * @param pieceN Piece number.
     * @return Found piece.
     */
    public Piece getPiece(int pieceN){
        return pieces.get(pieceN);
    }

    /**
     * <p>Get piece number at position.</p>
     * @param pos Position.
     * @return Piece number if found, -1 if not.
     */
    public int getPieceNum(int pos) {
        for (int i = 0; i < MAXPIECES; i++) {
            if (pieces.get(i).getPosition() == pos) { return i; }
        }
        return -1;
    }

    /**
     * <p>Get piece number at absolute position.</p>
     * @param pos Absolute position.
     * @return Piece number if found, -1 if not.
     */
    public int getPieceNumAbs(int pos) {
        for (int i = 0; i < MAXPIECES; i++) {
            if (Ludo.userGridToLudoBoardGrid(color, pieces.get(i).getPosition()) == pos) { return i; }
        }
        return -1;
    }

    /**
     * <p>Get number of s at given position.</p>
     * @param pos Position.
     * @return Number of player pieces at position.
     */
    public int getPiecesAt(int pos) {
        Iterator<Piece> iterator = pieces.iterator();
        int num = 0;

        while (iterator.hasNext()) {
            if (iterator.next().getPosition() == pos) { ++num; }
        }

        return num;
    }

    /**
     * <p>Get number of pieces at given absolute position.</p>
     * @param pos Absolute position.
     * @return Number of player pieces at position.
     */
    public int getPiecesAtAbs(int pos) {
        Iterator<Piece> iterator = pieces.iterator();
        int num = 0;

        while (iterator.hasNext()) {
            int localPos = iterator.next().getPosition();
            if (Ludo.userGridToLudoBoardGrid(color, localPos) == pos) { ++num; }
        }

        return num;
    }

    /**
     * <p>Amount of pieces that are on the ludo board.</p>
     * @return Number of pieces on the board.
     */
    public int piecesInGame() {
        Iterator<Piece> iterator = pieces.iterator();
        int num = 0;

        while (iterator.hasNext()) {
            if (iterator.next().getInGame()) { ++num; }
        }
        return num;
    }

    /**
     * <p>Checks if player is finished or not. Player is finished if all pieces are at the finish line</p>
     * @return True if finished, false if not.
     */
    public boolean isFinished() {
        Iterator<Piece> iterator = pieces.iterator();
        int num = 0;

        while (iterator.hasNext()) {
            if (iterator.next().getFinished()) { ++num; }
        }

        return (num == MAXPIECES);
    }

    /**
     * <p>Throw dice and set attempts.</p>
     * @param num Dice number thrown.
     * @return Attempts used
     */
    public int throwDiceAttempt(int num) {
        if (num == 6) {
            attempts = 0;
        }
        else if (++attempts == 3) {
            attempts = 0;
            return 3;
        }
        return attempts;
    }

    /**
     * <p>Throw dice and set sixes in a row.</p>
     * @param num Dice number thrown.
     * @return True if turn should be skipped, false if not.
     */
    public boolean throwDice(int num) {
        if (num != 6) { sixInRow = 0; }
        else { ++sixInRow; }

        if (sixInRow == 3) {
            sixInRow = 0;
            return true;
        }
        return false;
    }

    /**
     * <p>Set the status of the player.</p>
     * @param active Boolean, True if player is active, false if not.
     */
    public void setStatus(boolean active) {
        if (active) {
            status = "Active";
        }
        else {
            status ="Inactive";

            Iterator<Piece> iterator = pieces.iterator();
            while (iterator.hasNext()) {
                iterator.next().setPosition(0);
            }
        }
    }
}


package no.ntnu.imt3281.ludo.logic;

import java.util.Objects;

/**
 * <p>PlayerEvent class. Catched by implementers of the PlayerListener interface.</p>
 */
public class PlayerEvent {
    // PUBLIC STATIC CONSTANTS
    /**
     * <p>Constants for different statuses.</p>
     */
    public static final int
            WAITING = 1,
            PLAYING = 2,
            WON = 3,
            LEFTGAME = 4;

    // PUBLIC VARIABLES
    /**
     * <p>Origin board.</p>
     */
    public final Ludo ludo;
    /**
     * <p>Player color.</p>
     */
    public final int color;
    /**
     * <p>Player status.</p>
     */
    public final int status;

    // PUBLIC INTERFACE
    /**
     * <p>Constructor for player event.</p>
     * @param ludo Origin of the event.
     * @param color Color of the player.
     * @param status Status of the player.
     */
    public PlayerEvent(Ludo ludo, int color, int status) {
        this.ludo = ludo;
        this.color = color;

        if (status == WAITING ||
            status == PLAYING ||
            status == WON ||
            status == LEFTGAME
        ) {
            this.status = status;
        }
        else {
            this.status = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerEvent that = (PlayerEvent) o;
        return color == that.color &&
                status == that.status &&
                ludo.equals(that.ludo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ludo, color, status);
    }
}

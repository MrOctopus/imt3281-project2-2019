package no.ntnu.imt3281.ludo.logic;

/**
 * <p>Interface for implementation of PlayerListeners</p>
 */
public interface PlayerListener {
    void playerStateChanged(PlayerEvent event);
}

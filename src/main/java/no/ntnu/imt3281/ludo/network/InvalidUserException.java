package no.ntnu.imt3281.ludo.network;

/**
 * <p>Exception thrown in cases where the user cannot be found on the server.</p>
 */
public class InvalidUserException extends PacketException {
    // PUBLIC INTERFACE
    /**
     * <p>Constructor that allows for different types of packet errors.
     * It calls the super method of PacketException.</p>
     * @param error Packet that contains the error information.
     */
    public InvalidUserException(Packet error) {
        super("User is invalid", error);
    }
}

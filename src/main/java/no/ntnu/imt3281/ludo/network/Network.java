package no.ntnu.imt3281.ludo.network;

/**
 * <p>Shared client/server class, containing constants used by both parties.</p>
 */
public class Network {
    // PUBLIC STATIC CONSTANTS
    /**
     * <p>Default server port.</p>
     */
    public static final int DEFAULT_PORT = 4000;
    /**
     * <p>Default server host/ip.</p>
     */
    public static final String SERVER_HOST = "localhost";

    // PRIVATE INTERFACE
    private Network () {}
}

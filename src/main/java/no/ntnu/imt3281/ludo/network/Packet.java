package no.ntnu.imt3281.ludo.network;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.json.JSONObject;

/**
 * <p>Packet class used for storing data.</p>
 */
public class Packet {
    // CONSTANTS
    /**
     * <p>Packet type.</p>
     */
    public enum Type {
        /**
         * <p>Valid types.</p>
         */
        SESSION(0),
        MATCH(1),
        CHAT(2),
        GAME(3),
        TABLE(4);

        /**
         * <p>Ordinal independent index.</p>
         */
        private int index;

        // PUBLIC INTERFACE
        /**
         * <p>A type is created with an ordinal independent index,
         * to minimize conflicts due to differing enum orders.</p>
         * @param index Index of type.
         */
        Type (int index) {
            this.index = index;
        }

        /**
         * <p>Retrieve index.</p>
         * @return index
         */
        public int getIndex() {
            return index;
        }

        /**
         * <p>Retrieve enum for specified index.</p>
         * @param index Index to get Enum of.
         * @return event if exists, else null.
         */
        public static Type getEnum(int index) {
            for (Type t : Type.values()) {
                if (t.getIndex() == index) return t;
            }
            return null;
        }
    }

    /**
     * <p>Packet event.</p>
     */
    public enum Event {
        /**
         * <p>Valid events.</p>
         */
        ERROR(0),
        JOIN(1),
        LEAVE(2),
        CANCEL(3),
        CHALLENGE(4),
        INVALIDUSER(5),
        ACCEPT(6),
        CREATE(7),
        MSG(8),
        LOGIN(9),
        REGISTER(10),
        TOKEN(11),
        WINS(12),
        PLAYED(13),
        ROOMS(14),
        DICE(15),
        PIECE(16),
        FEWPLAYERS(17);

        /**
         * <p>Ordinal independent index.</p>
         */
        private int index;

        // PUBLIC INTERFACE
        /**
         * <p>An event is created with an ordinal independent index,
         * to minimize conflicts due to differing enum orders.</p>
         * @param index Index of event.
         */
        Event (int index) {
            this.index = index;
        }

        /**
         * <p>Retrieve index.</p>
         * @return index
         */
        public int getIndex() {
            return index;
        }

        /**
         * <p>Retrieve enum for specified index.</p>
         * @param index Index to get Enum of.
         * @return event if exists, else null.
         */
        public static Event getEnum(int index) {
            for (Event e : Event.values()) {
                if (e.getIndex() == index) return e;
            }
            return null;
        }
    }

    public static final String
            USER = "username",
            USERS = "users",
            CHATS = "chats",
            PASS = "password",
            TOKEN = "token",
            ID = "id",
            NR = "nr",
            CLOSE = "closeable",
            MSG = "msg";

    // PUBLIC VARIABLES
    public final Type type;
    public final Event event;
    public final JSONObject data;

    // PUBLIC INTERFACE
    /**
     * <p>Create new packet with the specified contents.</p>
     * @param type Type enum.
     * @param event Event enum.
     * @param json JSONObject. Can be null.
     */
    public Packet(Type type, Event event, JSONObject json) {
        this.type = type;
        this.event = event;
        this.data = json;
    }

    /**
     * <p>Read packet from stream. Note that thread synchronization should happen,
     * outside of the packet!</p>
     * @param br DataInputStream.
     * @throws IOException If reading fails.
     */
    public Packet(DataInputStream br) throws IOException {
        type = Type.getEnum(br.readInt());
        event = Event.getEnum(br.readInt());

        int length = br.readInt();
        if (length != 0) {
            byte[] bytes = new byte[length];
            StringBuilder jsonSB = new StringBuilder(length);

            while (length > 0) {
                int bytesRead = br.read(bytes);
                length -= bytesRead;
                jsonSB.append(new String(bytes, 0, bytesRead, StandardCharsets.UTF_8));
            }

            data = new JSONObject(jsonSB.toString());
        }
        else {
            data = null;
        }
    }

    /**
     * <p>Send created packet using given stream.
     * Note that thread synchronization should happen, outside of the packet!</p>
     * @param bw DataOutputStream.
     * @throws IOException If failed to write to output stream.
     */
    public void write(DataOutputStream bw) throws IOException {
        bw.writeInt(type.getIndex());
        bw.writeInt(event.getIndex());

        if (data != null) {
            byte[] stream = data.toString().getBytes(StandardCharsets.UTF_8);
            bw.writeInt(stream.length);
            bw.write(stream);
        }
        else {
            bw.writeInt(0);
        }

        bw.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Packet packet = (Packet) o;
        return type == packet.type &&
                event == packet.event &&
                Objects.equals(data, packet.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, event, data);
    }
}

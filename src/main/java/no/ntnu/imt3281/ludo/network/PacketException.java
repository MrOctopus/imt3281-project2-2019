package no.ntnu.imt3281.ludo.network;

import java.io.IOException;

/**
 * <p>Exception thrown when packet is corrupted or invalid.</p>
 */
public class PacketException extends IOException {
    // PRIVATE VARIABLES
    private final Packet error;

    // PUBLIC INTERFACE
    /**
     * <p>Default constructor, with default packet error.</p>
     */
    public PacketException() {
        super("Error occurred when handling packet");
        error = new Packet(Packet.Type.SESSION, Packet.Event.ERROR, null);
    }

    /**
     * <p>Retrieve the error packet associated with this exception.</p>
     * @return Packet error.
     */
    public Packet getError() {
        return error;
    }

    // PROTECTED INTERFACE
    protected PacketException(String msg, Packet error) {
        super(msg);
        this.error = error;
    }
}

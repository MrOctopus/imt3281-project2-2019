/**
 * This package includes all shared network classes
 */
/**
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
package no.ntnu.imt3281.ludo.network;
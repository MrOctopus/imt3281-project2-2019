package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Class that keeps track of all server chat rooms.</p>
 */
public class Chat {
    // PRIVATE STATIC VARIABLES
    private static final ConcurrentHashMap<String, Chat> rooms = new ConcurrentHashMap<>();

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Attempt retrieving a chat with a given name. If none exists, create
     * one with the specified closeable parameter.</p>
     * @param packet Packet containing the chat information.
     * @param closeable Closeable flag, client side.
     * @return Chat.
     * @throws PacketException If packet data is corrupt/invalid.
     */
    public static Chat getChat(Packet packet, boolean closeable) throws PacketException {
        try {
            String finalName = packet.data.getString(Packet.ID).toUpperCase();
            if (finalName.isEmpty()) { throw new PacketException(); }

            return rooms.computeIfAbsent(finalName, k -> new Chat(finalName, closeable));
        }
        catch (JSONException e) {
            throw new PacketException();
        }
    }

    /**
     * <p>Retrieve a table with the list of all current chatrooms.</p>
     * @return Packet containing the list of all chatrooms.
     */
    public static Packet getTable() {
        Set set = rooms.keySet();

        return new Packet(
                Packet.Type.TABLE,
                Packet.Event.ROOMS,
                new JSONObject().put(Packet.CHATS, new JSONArray(set.toArray(new String[set.size()])))
        );
    }

    // PRIVATE VARIABLES
    private final String name;
    private final boolean closeable;
    private final Logger log;
    private final Future msgThread;

    private final List<Session> sessions = new LinkedList<>();
    private final LinkedBlockingQueue<Packet> queue = new LinkedBlockingQueue<>();

    // PROTECTED INTERFACE
    protected void listener(Session sender, Packet packet) throws PacketException {
        switch (packet.event) {
            case JOIN:
                addClient(sender);
                break;
            case LEAVE:
                removeClient(sender);
                break;
            case MSG:
                sendMsg(sender.getName(), packet);
                break;
            default: throw new PacketException();
        }
    }

    protected String getName() {
        return name;
    }

    protected ArrayList getInRoom() {
        ArrayList<String> inRoom = new ArrayList<>();

        synchronized (sessions) {
            Iterator<Session> iterator = sessions.iterator();
            while (iterator.hasNext()) {
                inRoom.add(iterator.next().getName());
            }
        }

        return inRoom;
    }

    protected void addClient(Session session) {
        synchronized (sessions) {
            if (sessions.contains(session) || msgThread.isCancelled()) { return; }
            sessions.add(session);
            session.rooms.add(this);
        }

        Packet create = new Packet(
                Packet.Type.CHAT,
                Packet.Event.CREATE,
                new JSONObject().
                        put(Packet.ID, name).
                        put(Packet.CLOSE, closeable).
                        put(Packet.USERS, new JSONArray(getInRoom()))
        );

        Packet join = new Packet(
                Packet.Type.CHAT,
                Packet.Event.JOIN,
                new JSONObject().
                        put(Packet.ID, name).
                        put(Packet.USER, session.getName())
        );

        session.write(create);
        queue.add(join);
        log.log(Level.FINE, session.getName()+" JOINED");
    }

    protected void removeClient(Session session) {
        synchronized (sessions) {
            sessions.remove(session);
            session.rooms.remove(this);

            if (sessions.isEmpty() && closeable) {
                rooms.remove(name);
                msgThread.cancel(true);
                return;
            }
        }

        Packet leave = new Packet(
                Packet.Type.CHAT,
                Packet.Event.LEAVE,
                new JSONObject().
                        put(Packet.ID, name).
                        put(Packet.USER, session.getName())
        );

        queue.add(leave);
        log.log(Level.FINE, session.getName()+" LEFT");
    }

    // PRIVATE INTERFACE
    private Chat(String name, boolean closeable) {
        this.name = name;
        this.closeable = closeable;

        msgThread = Server.executor.submit(this::dispatcher);

        log = Logger.getLogger("Chat-"+name);
        log.setLevel(Level.FINE);
        log.log(Level.INFO, "Chatroom created");
    }

    private void sendMsg(String sender, Packet packet) throws PacketException {
        try {
            String msg = packet.data.getString(Packet.MSG);
            JSONObject data = new JSONObject();
            data.put(Packet.ID, name);
            data.put(Packet.USER, sender);
            data.put(Packet.MSG, msg);

            queue.add(new Packet(Packet.Type.CHAT, Packet.Event.MSG, data));
            log.log(Level.FINE, sender+" SENT MSG: "+msg);
        }
        catch (JSONException e) {
            throw new PacketException();
        }
    }

    private void dispatcher() {
        try {
            while (Server.connections != null) {
                final Packet packet = queue.take();

                synchronized (sessions) {
                    Iterator<Session> iterator = sessions.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().write(packet);
                    }
                }
            }
        }
        catch (InterruptedException e) {
            log.log(Level.INFO, "Chatroom dispatcher was interrupted. Chatroom will now be deleted");
            Thread.currentThread().interrupt();
        }
    }
}

package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.network.InvalidUserException;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>The server database. Note, the care only be one.</p>
 */
public class Database {
    // PRIVATE STATIC VARIABLES
    private static Database singleton = null;

    // PUBLIC STATIC INTERFACE

    /**
     * <p>Get database singleton. Attempt creating one if none exist.</p>
     * @return Database The singleton.
     */
    public static synchronized Database getSingleton() {
        if (singleton == null) { singleton = new Database(); }
        return singleton;
    }

    // PRIVATE VARIABLES
    private final Logger log = Logger.getLogger("Database");

    private Connection connection;

    // PROTECTED INTERFACE
    protected void shutdown() {
        if (connection == null) { return; }

        try {
            connection.close();
            connection = null;
            singleton = null;
        }
        catch (SQLException e) {
            log.log(Level.INFO, "Couldn't shutdown database properly");
        }
    }

    protected Packet getTopTen(Packet.Event event, String list) throws PacketException {
        try (Statement stmt = connection.createStatement();
             ResultSet set = stmt.executeQuery("SELECT username, "+list+" FROM USERS ORDER BY "+list+" DESC FETCH NEXT 10 ROWS ONLY");
        ) {
            JSONObject data = new JSONObject();
            JSONArray users = new JSONArray();

            while (set.next()) {
                users.put(set.getString("username"));
                users.put(set.getInt(list));
            }

            data.put(Packet.USERS, users);
            return new Packet(Packet.Type.TABLE, event, data);
        }
        catch(SQLException e) {
            shutdown();
            throw new PacketException();
        }
    }

    protected Packet getUser(Session session, Packet packet) throws PacketException {
        String strToken;

        try {
            strToken = packet.data.getString(Packet.TOKEN);
        }
        catch (JSONException e) {
            throw new PacketException();
        }

        byte[] token = token = Base64.getDecoder().decode(strToken);

        try (PreparedStatement prepSt = connection.prepareStatement(
            "SELECT username, token FROM TOKENS WHERE token=?")
        ) {
            prepSt.setBytes(1, token);

            try (ResultSet set = prepSt.executeQuery()) {
                if (!set.next()) {
                    throw new InvalidUserException(new Packet(
                            Packet.Type.SESSION,
                            Packet.Event.TOKEN,
                            null
                    ));
                }

                String username = set.getString("username");
                session.setName(username);

                return new Packet(
                        Packet.Type.SESSION,
                        Packet.Event.TOKEN,
                        new JSONObject().
                                put(Packet.USER, username).
                                put(Packet.TOKEN, strToken)
                );
            }
        }
        catch (SQLException e) {
            shutdown();
            throw new PacketException();
        }
    }

    protected void updateStats(String username, boolean won) {
        try (PreparedStatement prepSt = connection.prepareStatement(
                "UPDATE USERS SET wins = wins +"+(won ? 1 : 0)+", played = played + 1 WHERE username=?"
        )
        ) {
            prepSt.setString(1, username);
            prepSt.executeUpdate();
        }
        catch (SQLException e) {
            shutdown();
        }
    }

    protected Packet registerUser(Session session, Packet packet) throws PacketException {
        String username;
        String password;

        try {
            username = packet.data.getString(Packet.USER);
            password = packet.data.getString(Packet.PASS);
        }
        catch (JSONException e) {
            throw new PacketException();
        }

        try (PreparedStatement prepSt = connection.prepareStatement(
                "INSERT INTO USERS (username, pass, salt) VALUES (?,?,?)");
             PreparedStatement prepSt2 = connection.prepareStatement(
                     "INSERT INTO TOKENS (username, token) VALUES (?,?)")
        ) {
            if (userExists(username)) {
                throw new InvalidUserException(new Packet(
                        Packet.Type.SESSION,
                        Packet.Event.REGISTER,
                        null
                ));
            }

            byte[] salt = Utility.genRandom(32);
            byte[] hashedPass = Utility.hashSHA256(password, salt);
            byte[] token = Utility.hashSHA256(username, Utility.genRandom(20));

            prepSt.setString(1, username);
            prepSt.setBytes(2, hashedPass);
            prepSt.setBytes(3, salt);
            prepSt2.setString(1, username);
            prepSt2.setBytes(2, token);

            prepSt.executeUpdate();
            prepSt2.executeUpdate();

            session.setName(username);
            return new Packet(
                    Packet.Type.SESSION,
                    Packet.Event.TOKEN,
                    new JSONObject().
                            put(Packet.USER, username).
                            put(Packet.TOKEN, Base64.getEncoder().encodeToString(token))
            );
        }
        catch (SQLException e) {
            shutdown();
            throw new PacketException();
        }
    }

    protected Packet loginUser (Session session, Packet packet) throws PacketException {
        String username;
        String password;

        try {
            username = packet.data.getString(Packet.USER);
            password = packet.data.getString(Packet.PASS);
        }
        catch (JSONException e) {
            throw new PacketException();
        }

        try (PreparedStatement prepSt = connection.prepareStatement(
                "SELECT username, pass, salt FROM USERS WHERE username=?");
             PreparedStatement prepSt2 = connection.prepareStatement(
                     "SELECT token FROM TOKENS WHERE username=?")
        ) {
            Packet failed = new Packet(
                    Packet.Type.SESSION,
                    Packet.Event.LOGIN,
                    null
            );

            if (!userExists(username)) { throw new InvalidUserException(failed); }

            prepSt.setString(1, username);
            prepSt2.setString(1, username);

            try (ResultSet set = prepSt.executeQuery()) {
                set.next();

                byte[] salt = set.getBytes("salt");
                byte[] passwordBytes = Utility.hashSHA256(password, salt);

                if (!Arrays.equals(passwordBytes, set.getBytes("pass"))) {
                    throw new InvalidUserException(failed);
                }
            }

            try (ResultSet set = prepSt2.executeQuery()) {
                set.next();

                session.setName(username);
                return new Packet(
                        Packet.Type.SESSION,
                        Packet.Event.TOKEN,
                        new JSONObject().
                                put(Packet.USER, username).
                                put(Packet.TOKEN, Base64.getEncoder().encodeToString(set.getBytes("token")))
                );
            }
        }
        catch (SQLException e) {
            shutdown();
            throw new PacketException();
        }
    }

    // PRIVATE INTERFACE
    private Database() {
        try {
            connection = DriverManager.getConnection("jdbc:derby:LUDOdb");
        }
        catch (SQLException e) {
            log.log(Level.SEVERE, "Couldn't establish database connection, attempting to create new");

            try {
                connection = DriverManager.getConnection("jdbc:derby:LUDOdb;create=true");
                sendSimpleStatement("CREATE TABLE USERS (" +
                                    "username VARCHAR(64) NOT NULL PRIMARY KEY," +
                                    "pass VARCHAR(32) FOR BIT DATA NOT NULL," +
                                    "salt VARCHAR(32) FOR BIT DATA NOT NULL," +
                                    "played INTEGER DEFAULT 0," +
                                    "wins INTEGER DEFAULT 0)");
                sendSimpleStatement("CREATE TABLE TOKENS (" +
                                    "username VARCHAR(64) NOT NULL REFERENCES USERS(username)," +
                                    "token VARCHAR(32) FOR BIT DATA NOT NULL)");
                log.log(Level.INFO, "Created database successfully");
            }
            catch (SQLException e2) {
                log.log(Level.SEVERE, "Couldn't create database");
                System.exit(1);
            }
        }
    }

    private boolean userExists(String username) throws SQLException {
        try (PreparedStatement query = connection.prepareStatement(
                "SELECT username FROM USERS WHERE username=?")
        ) {
            query.setString(1, username);

            try (ResultSet set = query.executeQuery()) {
                if (set.next()) { return true; }
            }
        }

        return false;
    }

    private void sendSimpleStatement(String statement) throws SQLException {
        try (Statement stmt = connection.createStatement()) {
            stmt.execute(statement);
        }
    }
}

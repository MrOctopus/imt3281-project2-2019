package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.logic.*;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Game class that keeps track of all ongoing games.</p>
 */
public class Game implements DiceListener, PieceListener, PlayerListener {
    // PRIVATE STATIC VARIABLES
    private static final ConcurrentHashMap<String, Game> games = new ConcurrentHashMap<>();

    // PUBLIC STATIC INTERFACE
    /**
     * <p>Get a game object using the provided id in the packet.</p>
     * @param packet Packet containing the id.
     * @return Game with the matching UUID
     * @throws PacketException If the packet is malformed, or the game couldn't be found.
     */
    public static Game getGame(Packet packet) throws PacketException {
        try {
            String id = packet.data.getString(Packet.ID);
            Game game = games.get(id);
            if (game == null) { throw new PacketException(); }

            return game;
        }
        catch (JSONException e) {
            throw new PacketException();
        }
    }

    /**
     * <p>Generate a new uuid and use the provided ludo object.</p>
     * @param ludo The game to be hosted.
     */
    public static void addGame(Ludo ludo) {
        String id = Utility.genUUID();
        games.put(id, new Game(id, ludo));
    }

    // PRIVATE VARIABLES
    private final String id;
    private final Future eventThread;

    private final Ludo ludo;
    private final Logger log;

    private final List<Session> sessions = Collections.synchronizedList(new ArrayList<Session>(Ludo.MAXPLAYERS));
    private final LinkedBlockingQueue<Packet> queue = new LinkedBlockingQueue<>();

    // PUBLIC INTERFACE
    /**
     * <p>Implements diceThrown from diceListener.</p>
     * @param event DiceEvent.
     */
    public void diceThrown(DiceEvent event) {
        queue.add(new Packet(
           Packet.Type.GAME,
           Packet.Event.DICE,
           new JSONObject().
                   put(Packet.ID, id).
                   put(Packet.NR, event.nr)
        ));
    }

    /**
     * <p>Implements pieceMoved from pieceListener.</p>
     * @param event PieceEvent.
     */
    public void pieceMoved(PieceEvent event) {
        queue.add(new Packet(
                Packet.Type.GAME,
                Packet.Event.PIECE,
                new JSONObject().
                        put(Packet.ID, id).
                        put(Packet.NR, event.color).
                        put(Packet.NR+1, event.from).
                        put(Packet.NR+2, event.to)
        ));
    }

    /**
     * <p>Implements playerStateChanged from playerListener.</p>
     * @param event PlayerEvent.
     */
    public void playerStateChanged(PlayerEvent event) {
        if (event.status == PlayerEvent.WON) {
            int size = ludo.nrOfPlayers();

            for (int i = 0; i < size; i++) {
                String username = ludo.getPlayerName(0);

                if (username != null && !username.startsWith("Inactive")) {
                    Database.getSingleton().updateStats(username,  event.color == i);
                }
            }
        }
        else if (event.status == PlayerEvent.LEFTGAME) {
            queue.add(new Packet(
                    Packet.Type.GAME,
                    Packet.Event.LEAVE,
                    new JSONObject().
                            put(Packet.ID, id).
                            put(Packet.NR, event.color)
            ));
        }
    }

    // PROTECTED INTERFACE
    protected void listener(Session session, Packet packet) throws PacketException {
        // Non-game events
        if (packet.event == Packet.Event.LEAVE) {
            removeClient(session);
            return;
        }

        if (packet.event == Packet.Event.MSG) {
            sendMsg(session.getName(), packet);
            return;
        }

        // Game events
        handleGameEvent(session, packet);
    }

    protected void removeClient(Session session) {
        sessions.remove(session);
        session.games.remove(this);

        synchronized (ludo) {
            ludo.removePlayer(session.getName());
            if (ludo.activePlayers() == 0) {
                games.remove(id);
                eventThread.cancel(true);
                return;
            }
        }

        log.log(Level.FINE, session.getName()+" LEFT");
    }

    // PRIVATE INTERFACE
    private Game(String id, Ludo ludo) {
        this.id = id;
        this.ludo = ludo;

        ludo.addDiceListener(this);
        ludo.addPieceListener(this);
        ludo.addPlayerListener(this);

        eventThread = Server.executor.submit(this::dispatcher);

        JSONArray users = new JSONArray();
        int size = ludo.nrOfPlayers();

        for (int i = 0; i < size; i++) {
            String username = ludo.getPlayerName(i);

            try {
                Session session = Session.getClient(username);
                sessions.add(session);
                session.games.add(this);
                users.put(username);
            }
            catch (UserNotFoundException e) {
                ludo.removePlayer(username);
            }
        }

        queue.add(new Packet(
                Packet.Type.GAME,
                Packet.Event.CREATE,
                new JSONObject().
                        put(Packet.ID, id).
                        put(Packet.USERS, users)
        ));


        log = Logger.getLogger("Game-"+id);
        log.setLevel(Level.FINE);
        log.log(Level.INFO, "Game created");
    }

    private void sendMsg(String sender, Packet packet) throws PacketException {
        try {
            String msg = packet.data.getString(Packet.MSG);
            JSONObject data = new JSONObject();
            data.put(Packet.ID, id);
            data.put(Packet.USER, sender);
            data.put(Packet.MSG, msg);

            queue.add(new Packet(Packet.Type.GAME, Packet.Event.MSG, data));
            log.log(Level.FINE, sender+" SENT MSG: "+msg);
        }
        catch (JSONException e) {
            throw new PacketException();
        }
    }

    private void handleGameEvent(Session session, Packet packet) throws PacketException {
        synchronized (ludo) {
            if (!ludo.getPlayerName(ludo.activePlayer()).equals(session.getName())) { return; }
            if (ludo.getWinner() != -1) { return; }

            if (packet.event == Packet.Event.DICE) {
                ludo.throwDice(ludo.throwDice());
                return;
            }

            if (packet.event == Packet.Event.PIECE) {
                int from;
                int to;

                try {
                    from = ludo.getPosition(ludo.activePlayer(), packet.data.getInt(Packet.NR));
                    to = from+ludo.getLastThrow();
                }
                catch (JSONException e) {
                    throw new PacketException();
                }

                if (from != 0 || to == 6) {
                    ludo.movePiece(ludo.activePlayer(), from, (from == 0) ? 1 : to);
                }

                return;
            }
        }

        // DEFAULT
        throw new PacketException();
    }

    private void dispatcher() {
        try {
            while (Server.connections != null) {
                final Packet packet = queue.take();

                synchronized (sessions) {
                    Iterator<Session> iterator = sessions.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().write(packet);
                    }
                }
            }
        }
        catch (InterruptedException e) {
            log.log(Level.INFO, "Game dispatcher was interrupted. Game will now be deleted");
            Thread.currentThread().interrupt();
        }
    }
}

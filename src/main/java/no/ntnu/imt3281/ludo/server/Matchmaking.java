package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.NoRoomForMorePlayersException;
import no.ntnu.imt3281.ludo.logic.NotEnoughPlayersException;
import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * <p>Matchmaking class that handles all matchmaking between sessions/clients.</p>
 */
public class Matchmaking {
    // PRIVATE STATIC VARIABLES
    private static final ConcurrentHashMap<String, Ludo> beingHosted = new ConcurrentHashMap<>();
    private static final LinkedBlockingQueue<Session> wantToJoin = new LinkedBlockingQueue<>();
    private static final LinkedBlockingQueue<Session> waitingQueue = new LinkedBlockingQueue<>();

    private static Future queueThread;

    // PROTECTED STATIC INTERFACE
    protected static void listener(Session session, Packet packet) throws PacketException {
        switch (packet.event) {
            case CHALLENGE:
                challenge(session, packet);
                break;
            case ACCEPT:
                challengeAccept(session ,packet);
                break;
            case JOIN:
                addClient(session);
                break;
            case CANCEL:
                removeClient(session);
                break;
            default: throw new PacketException();
        }
    }

    protected static void addClient(Session session) {
        if (wantToJoin.contains(session) || waitingQueue.contains(session)) { return; }
        wantToJoin.add(session);
    }

    protected static void removeClient(Session session) {
        beingHosted.remove(session.getName());
        wantToJoin.remove(session);
        waitingQueue.remove(session);
    }

    protected static void queueGame() {
        try {
            while (Server.connections != null) {
                final Session session = wantToJoin.take();
                beingHosted.remove(session.getName());
                waitingQueue.add(session);

                if (queueThread != null) { queueThread.cancel(false); }
                if (waitingQueue.size() >= Ludo.MAXPLAYERS) { newQueueGame(); }
                else {
                    queueThread = Server.scheduler.schedule(Matchmaking::cancelQueue, 30, TimeUnit.SECONDS);
                }
            }
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    // PRIVATE STATIC INTERFACE
    private static void challenge(Session session, Packet packet) throws PacketException {
        String username;
        Session otherSession;

        try {
            username = packet.data.getString(Packet.USER);
        }
        catch (JSONException e) {
            throw new PacketException();
        }

        try {
            otherSession = Session.getClient(username);
            if (session == otherSession) { throw new UserNotFoundException(); }
        }
        catch (UserNotFoundException e) {
            Packet invalidUser = new Packet(
                    Packet.Type.MATCH,
                    Packet.Event.INVALIDUSER,
                    new JSONObject().put(Packet.USER, username)
            );

            session.write(invalidUser);
            return;
        }

        if (session.challenges.contains(otherSession)) { return; }

        session.challenges.add(otherSession);
        beingHosted.computeIfAbsent(session.getName(), k ->{
            Server.scheduler.schedule(()->challengeExpired(session), 30, TimeUnit.SECONDS);
            Ludo ludo = new Ludo();

            try {
                ludo.addPlayer(session.getName());
            }
            catch (NoRoomForMorePlayersException e) {
                // Won't happen
            }

            return ludo;
        });

        Packet challenge = new Packet(
                Packet.Type.MATCH,
                Packet.Event.CHALLENGE,
                new JSONObject().put(Packet.USER, session.getName())
        );

        otherSession.write(challenge);
    }

    private static void challengeAccept(Session session, Packet packet) throws PacketException {
        String username;
        Session otherSession;

        try {
            username = packet.data.getString(Packet.USER);
        }
        catch (JSONException e) {
            throw new PacketException();
        }

        try {
            otherSession = Session.getClient(username);
        }
        catch (UserNotFoundException e) {
            Packet invalidUser = new Packet(
                    Packet.Type.MATCH,
                    Packet.Event.INVALIDUSER,
                    new JSONObject().put(Packet.USER, username)
            );

            session.write(invalidUser);
            return;
        }

        if (!otherSession.challenges.contains(session)) { return; }
        Ludo ludo = beingHosted.get(otherSession.getName());
        if (ludo == null) { return; }

        synchronized (ludo) {
            try {
                ludo.addPlayer(session.getName());
                if (ludo.nrOfPlayers().equals(Ludo.MAXPLAYERS)) {
                    beingHosted.remove(otherSession.getName());
                    Game.addGame(ludo);
                }
            }
            catch (NoRoomForMorePlayersException e) {
                // DO NOTHING
            }
        }
    }

    private static void challengeExpired(Session session) {
        session.challenges.clear();
        Ludo ludo = beingHosted.remove(session.getName());

        if (ludo == null) { return; }

        synchronized (ludo) {
            int size = ludo.nrOfPlayers();

            if (size < 2) {
                Packet tooFewPlayers = new Packet(
                        Packet.Type.MATCH,
                        Packet.Event.FEWPLAYERS,
                        null
                );

                session.write(tooFewPlayers);
            }
            else if (size < Ludo.MAXPLAYERS) {
                Game.addGame(ludo);
            }
        }
    }

    private static void cancelQueue() {
        queueThread = null;
        synchronized (waitingQueue) {
            Iterator<Session> iterator = waitingQueue.iterator();
            while (iterator.hasNext()) {
                iterator.next().write(new Packet(
                        Packet.Type.MATCH,
                        Packet.Event.CANCEL,
                        null
                ));
            }
            waitingQueue.clear();
        }
    }

    private static void newQueueGame() {
        try {
            Game.addGame(new Ludo(
                    waitingQueue.take().getName(),
                    waitingQueue.take().getName(),
                    waitingQueue.take().getName(),
                    waitingQueue.take().getName()
            ));
        }
        catch (NotEnoughPlayersException e) {
            // Should not happen. If it does just ignore it.
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    // PRIVATE INTERFACE
    private Matchmaking() {}
}

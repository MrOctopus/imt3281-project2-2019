package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.network.Network;

import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * This is the main class for the server. 
 * **Note, change this to extend other classes if desired.**
 *
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
public class Server {
	// PUBLIC STATIC CONSTANTS
	/**
	 * <p>Constant for thread threshold of new concurrenthashmap
	 * foreach threads.</p>
	 */
	public static final int THRESHOLD_THREAD = 70;
	/**
	 * <p>Constant for the name of the global chat.</p>
	 */
	public static final String GLOBAL = "GLOBAL";

	// PROTECTED STATIC VARIABLES
	protected static final ExecutorService executor = Executors.newCachedThreadPool();
	protected static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);

	protected static ServerSocket connections;

	// PRIVATE STATIC VARIABLES
	private static final Logger log = Logger.getLogger("Server");

	// PUBLIC STATIC INTERFACE
	/**
	 * <p>Main function that starts the server.</p>
	 * @param args Cmd arguments.
	 */
	public static void main(String[] args) {
		Database.getSingleton();

		try {
			connections = new ServerSocket(Network.DEFAULT_PORT);
		}
		catch (IOException e) {
			log.log(Level.SEVERE, "Couldn't establish server connection");
			shutdown();
			System.exit(2);
		}

		// Listeners
		executor.execute(Server::listener);
		executor.execute(Session::listener);
		executor.execute(Matchmaking::queueGame);
	}

	/**
	 * <p>Shutdown the entire server "Gracefully".</p>
	 */
	public static void shutdown() {
		try {
			executor.shutdown();
			connections.close();
			connections = null;
		}
		catch (IOException e) {
			log.log(Level.SEVERE, "Could not shut down the server gracefully.");
		}
		Database.getSingleton().shutdown();
	}

	// PRIVATE STATIC INTERFACE
	private static void listener() {
		while (connections != null) {
			try {
				log.log(Level.INFO, "Accepting new connections");
				Session.addClient(connections.accept());
			}
			catch (IOException e) {
				log.log(Level.INFO, e.getMessage());
			}
		}
	}
}
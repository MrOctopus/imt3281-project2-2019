package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>The session class handles all packets from sessions, and keeps track of client data.</p>
 */
public class Session {
    // PRIVATE STATIC VARIABLES
    private static final Logger log = Logger.getLogger("Sessions");
    private static final ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();

    // PUBLIC STATIC INTERFACE

    /**
     * <p>Retrieve client/session of the given username.</p>
     * @param username The user's username.
     * @return Session The user session.
     * @throws UserNotFoundException If user couldn't be found.
     */
    public static Session getClient(String username) throws UserNotFoundException {
        Session session = sessions.get(username);
        if (session == null) { throw new UserNotFoundException(); }
        return session;
    }

    /**
     * <p>Add a new client/session, using the given socket.</p>
     * @param connection Assigned user socket.
     * @throws IOException If login/register failed, packets were corrupt or
     * the user connection dropped.
     */
    public static void addClient(Socket connection) throws IOException {
        Session session = new Session(connection);
        Session oldSession = sessions.get(session.getName());

        if (oldSession != null) { disconnect(oldSession); }
        sessions.put(session.getName(), session);
        log.log(Level.INFO, "Client connected");

        Packet joinGlobal = new Packet(
                Packet.Type.CHAT,
                Packet.Event.JOIN,
                new JSONObject().put(Packet.ID, Server.GLOBAL)
        );

        Chat.getChat(joinGlobal, false).listener(session, joinGlobal);
    }

    // PROTECTED STATIC INTERFACE
    protected static void listener() {
        while (Server.connections != null) {
            sessions.forEachValue(Server.THRESHOLD_THREAD, session -> {
                Packet packet = session.read();

                if (packet != null) {
                    try {
                        switch (packet.type) {
                            case SESSION:
                                childListener(session, packet);
                                break;
                            case MATCH:
                                Matchmaking.listener(session, packet);
                                break;
                            case TABLE:
                                Table.listener(session, packet);
                                break;
                            case CHAT:
                                Chat.getChat(packet, true).listener(session, packet);
                                break;
                            case GAME:
                                Game.getGame(packet).listener(session, packet);
                                break;
                            default: throw new PacketException();
                        }
                    }
                    catch (PacketException e) {
                        session.write(e.getError());
                        log.log(Level.INFO, e.getMessage());
                    }
                }
            });
        }
    }

    // PRIVATE STATIC INTERFACE
    private static void childListener(Session session, Packet packet) throws PacketException {
        switch (packet.event) {
            case LEAVE:
                disconnect(session);
                break;
            default: throw new PacketException();
        }
    }

    private static void disconnect(Session session) {
        if (sessions.remove(session.getName()) == null) { return; }
        session.close();
    }

    // PROTECTED VARIABLES
    protected final LinkedBlockingQueue<Chat> rooms = new LinkedBlockingQueue<>();
    protected final LinkedBlockingQueue<Game> games = new LinkedBlockingQueue<>();
    protected final List<Session> challenges = Collections.synchronizedList(new ArrayList<Session>());

    // PRIVATE VARIABLES
    private String username;

    private Socket connection;
    private DataOutputStream bw;
    private DataInputStream br;

    // PROTECTED INTERFACE
    protected String getName() {
        return username;
    }

    protected void setName(String username) {
        this.username = username;
    }

    protected synchronized Packet read() {
        try {
            if (br.available() != 0) { return new Packet(br); }
        }
        catch (IOException e) {
            Server.executor.execute(()->disconnect(this));
        }
        return null;
    }

    protected synchronized void write(Packet packet) {
        try {
            packet.write(bw);
        } catch (IOException e) {
            Server.executor.execute(()->disconnect(this));
        }
    }

    // PRIVATE INTERFACE
    private Session(Socket connection) throws IOException {
        this.connection = connection;

        bw = new DataOutputStream(new BufferedOutputStream(connection.getOutputStream()));
        br = new DataInputStream(new BufferedInputStream(connection.getInputStream()));

        Packet packet = new Packet(br);

        try {
            if (packet.type != Packet.Type.SESSION) { throw new PacketException(); }
            Packet reply;

            switch (packet.event) {
                case TOKEN:
                    reply = Database.getSingleton().getUser(this, packet);
                    break;
                case LOGIN:
                    reply = Database.getSingleton().loginUser(this, packet);
                    break;
                case REGISTER:
                    reply = Database.getSingleton().registerUser(this, packet);
                    break;
                default:
                    throw new PacketException();
            }

            reply.write(bw);
        }
        catch (PacketException e) {
            e.getError().write(bw);
            throw e;
        }
    }

    private void close() {
        try {
            connection.close();
            connection = null;
        }
        catch (IOException e) {
            log.log(Level.INFO, "Client failed to disconnect properly");
        }

        Matchmaking.removeClient(this);

        Chat chat = rooms.peek();
        while (chat != null) {
            chat.removeClient(this);
            chat = rooms.peek();
        }

        Game game = games.peek();
        while (game != null) {
            game.removeClient(this);
            game = games.peek();
        }

        log.log(Level.INFO, "Client was disconnected");
    }
}

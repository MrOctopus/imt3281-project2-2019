package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.network.Packet;
import no.ntnu.imt3281.ludo.network.PacketException;

/**
 * <p>Server class that retrieves tables from different sources.</p>
 */
public class Table {
    // PROTECTED STATIC INTERFACE
    protected static void listener(Session session, Packet packet) throws PacketException {
        switch (packet.event) {
            case WINS:
                session.write(Database.getSingleton().getTopTen(Packet.Event.WINS, "wins"));
                break;
            case PLAYED:
                session.write(Database.getSingleton().getTopTen(Packet.Event.PLAYED, "played"));
                break;
            case ROOMS:
                session.write(Chat.getTable());
                break;
            default: throw new PacketException();
        }
    }

    // PRIVATE INTERFACE
    private Table() {}
}

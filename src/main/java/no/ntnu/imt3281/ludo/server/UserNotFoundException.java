package no.ntnu.imt3281.ludo.server;

/**
 * <p>Exception thrown when users can't be found in the hashmap.</p>
 */
public class UserNotFoundException extends Exception {
    /**
     * <p>Default constructor</p>
     */
    public UserNotFoundException() {
        super("User is not online or does not exist");
    }
}

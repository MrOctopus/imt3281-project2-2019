package no.ntnu.imt3281.ludo.server;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import java.util.UUID;

/**
 * <p>Utility class used sever side to generate UUIDs, Random bytes and hashes</p>
 */
public class Utility {
    // PRIVATE STATIC CONSTANTS
    private static final Random GENERATOR = new SecureRandom();

    // PUBLIC STATIC INTERFACE

    /**
     * <p>Generate a random set of bytes using SecureRandom.</p>
     * @param num Number of bytes.
     * @return byte[] Array of bytes.
     */
    public static byte[] genRandom(int num) {
        byte[] random = new byte[num];
        GENERATOR.nextBytes(random);
        return random;
    }

    /**
     * <p>Generate a uuid.</p>
     * @return UUID in string format.
     */
    public static String genUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * <p>Has the given string and salt with 4 iterations.</p>
     * @param toHash String to be hashed.
     * @param salt Salt to be used when hashing.
     * @return byte[] Hashed string.
     */
    public static byte[] hashSHA256(String toHash, byte[] salt) {
        try{
            PBEKeySpec spec = new PBEKeySpec(toHash.toCharArray(), salt, 4, 256);
            SecretKey key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(spec);
            return key.getEncoded();
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            // Will not happen
            throw new RuntimeException(e);
        }
    }

    // PRIVATE INTERFACE
    private Utility() {}
}

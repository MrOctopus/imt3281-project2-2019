/**
 * This package includes all purely server related classes
 */
/**
 * @author Fredrik Wilhelm Reite
 * @author Philip Ricanek
 * @author Magnus Onsrud
 *
 */
package no.ntnu.imt3281.ludo.server;
package no.ntnu.imt3281.ludo.network;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * <p>Tests to check if the packet class works properly.</p>
 */
public class PacketTest {
    @Test
    /**
     * <p>Test to see if the getIndex function returns the correct index.</p>
     */
    public void getIndexTest() {
        Packet packet = new Packet(
                Packet.Type.GAME,
                Packet.Event.CREATE,
                null
        );

        assertEquals(3, packet.type.getIndex());
        assertEquals(7, packet.event.getIndex());
    }

    @Test
    /**
     * <p>Test to see if the getEnum functions work.</p>
     */
    public void getEnumTest() {
        assertEquals(Packet.Type.GAME, Packet.Type.getEnum(3));
        assertEquals(Packet.Event.CHALLENGE, Packet.Event.getEnum(4));
    }

    @Test
    /**
     * <p>Test to see if the write and read functions work properly.</p>
     */
    public void writeReadTest() {
        Packet packet = new Packet(
                Packet.Type.SESSION,
                Packet.Event.LEAVE,
                null
        );
        Packet packet2 = null;

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        boolean exceptionThrown = false;
        try {
            packet.write(new DataOutputStream(output));
            packet2 = new Packet(new DataInputStream(new ByteArrayInputStream(output.toByteArray())));
        }
        catch (IOException e) {
            exceptionThrown = true;
        }
        assertFalse(exceptionThrown);
        assertTrue(packet.equals(packet2));
    }
}
